<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Seplat;

class SeplatController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $ref
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $ref)
    {
        return Seplat::where('dpr_ref', $ref)->get();
    }
}
