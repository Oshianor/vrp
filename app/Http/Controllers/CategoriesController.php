<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoriesController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $vendorId
     * @return \Illuminate\Http\Response
     */
    public function index($vendorId)
    {
        return Category::where('vendor_id', $vendorId)->get();
    }
}
