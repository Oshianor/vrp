<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subcat;

class SubcatController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $subcat_ref
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $subcat_ref)
    {
        return Subcat::where('ref', $subcat_ref)->get();
    }
}
