<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Subcategory;

class SubcategoriesController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $vendorId
     * @param  $dprRef
     * @param  $seplat
     * @return \Illuminate\Http\Response
     */
    public function index($vendorId, $dprRef, $seplat)
    {
        $exist = Category::
            where('vendor_id', $vendorId)
            ->where('dpr_ref', $dprRef)->first();
        $subext = Subcategory::where('seplat_ref', $seplat)->first();
        if (!$exist) {
            $cat = new Category;
            $cat->vendor_id = $vendorId;
            $cat->dpr_ref = $dprRef;
            if ($cat->save()) {
                
                if (!$subext) {
                    $sub = new Subcategory;
                    $sub->category_id = $cat->id;
                    $sub->seplat_ref = $seplat;
                    $sub->save();
                }
                
            }
        } else {
            if (!$subext) {
                $sub = new Subcategory;
                $sub->category_id = $exist->id;
                $sub->seplat_ref = $seplat;
                $sub->save();
            }
        }
        // return $exist->id;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $seplat
     * @return \Illuminate\Http\Response
     */
    public function remove($seplat)
    {
        // $cat = Category::where('vendor_id', $vendorId)->where('dpr_ref', $dprRef)->first();

        // $sub = new Subcategory;
        // $sub->category_id = $exist->id;
        // $sub->seplat_ref = $seplat;
        // $sub->save();
        $sub = Subcategory::where('seplat_ref', $seplat)->first();
        if ($sub) {
            $num = Subcategory::where('category_id', $sub->category_id)->count();
            if ($num >= 2) {
                $sub->delete();
                return "Done";
                
            } else {
                Category::where('id', $sub->category_id)->delete();
                $sub->delete();
                return "Done";
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $vendorId
     * @param  $dprRef
     * @return \Illuminate\Http\Response
     */
    public function subcat($vendorId, $dprRef)
    {
         $exist = Category::where('vendor_id', $vendorId)
            ->where('dpr_ref', $dprRef)->first();
        if ($exist) {
            return Subcategory::
            where('category_id', $exist->id)->get();
        }
        
    }
}
