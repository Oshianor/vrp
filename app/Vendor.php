<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    //
    public $created = "created_at";
    public $updated = "updated_at";
}
