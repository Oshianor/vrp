<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use App\Vendor;


Route::get('/', "VendorController@index");
Route::post('/login', "VendorController@store");
Route::get('/api/dpr', "DprController@index");
Route::get('/api/seplat/{ref}', "SeplatController@index");
Route::get('/api/subcat/{subcat_ref}', "SubcatController@index");
Route::get('/add/seplat/{vendor_id}/{dpr}/{checked}', "SubcategoriesController@index");
Route::get('/remove/seplat/{checked}', "SubcategoriesController@remove");
Route::get('/subcat/{vendor_id}/{dpr}', "SubcategoriesController@subcat");
Route::post('/upload', "VendorController@create");
Route::get('/get/categories/{vendor_id}', "CategoriesController@index");


Route::post('/send/email', function (Request $request) {
    $vendorName = $request->input('vendor');
    $vendorEmail = $request->input('vendorEmail');
    $type = $request->input('type');
    $cvrf = $request->input('cvrf');
    $bankRef = $request->input('bankRef');
    $bankDetails = $request->input('bankDetails');
    $lastAudit = $request->input('lastAudit');
    $certIncorp = $request->input('certIncorp');
    $formCOTWO = $request->input('formCOTWO');
    $proofTax = $request->input('proofTax');
    $incomeTax = $request->input('incomeTax');
    $companyProfile = $request->input('companyProfile');
    $proofMedical = $request->input('proofMedical');
    $dprPermit = $request->input('dprPermit');
    $introLetter = $request->input('introLetter');
    $workmen = $request->input('workmen');
    $thirdParty = $request->input('thirdParty');
    $hssePolicy = $request->input('hssePolicy');
    $assurance = $request->input('assurance');
    $agency = $request->input('agency');
    $memorandum = $request->input('memorandum');
    
    $data = [
        "vendor" => $vendorName,
        "vendorEmail" => $vendorEmail,
        "type" => $type,
        "cvrf" => $cvrf,
        "bankRef" => $bankRef,
        "bankDetails" => $bankDetails,
        "lastAudit" => $lastAudit,
        "certIncorp" => $certIncorp,
        "formCOTWO" => $formCOTWO,
        "memorandum" => $memorandum,
        "proofTax" => $proofTax,
        "incomeTax" => $incomeTax,
        "companyProfile" => $companyProfile,
        "proofMedical" => $proofMedical,
        "dprPermit" => $dprPermit,
        "introLetter" => $introLetter,
        "workmen" => $workmen,
        "thirdParty" => $thirdParty,
        "hssePolicy" => $hssePolicy,
        "assurance" => $assurance,
        "agency" => $agency,
    ];
    if ($vendorEmail !== "") {
        Mail::send('mail', $data, function ($message)  use ($vendorEmail, $vendorName) {
            $message->to($vendorEmail, $vendorName)
                ->to("seplatrevalidation@gmail.com", "Seplat")
                ->subject('Document Confirmation Email');
            $message->from("seplatrevalidation@gmail.com", "Seplat");
        });
    }
    
    // echo $request;
});

Route::get('/download', function () {
    $file = public_path()."/sep.docx";
    $header = [
        "Content-Type: application/docx",
    ];
    return Response::download($file, "Vendor Registration Form.docx", $header);
});
// google drive api

// Completed Vendor Registration Form
Route::post('/cvrf', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $cvrf = $request->file('cvrf'); 
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($cvrf) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
            $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
                // ->where('filename', '=', 'Seplat Vendor Data')
                // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Completed Vendor Registration Form')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Completed Vendor Registration Form');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Completed Vendor Registration Form')
            ->first(); // There could be duplicate directory names!
        foreach ($cvrf as $file) {
            // $filer = $request->file('myFile');
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// Certificate of Incorpoation / Registration
Route::post('/certIncorp', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $upload = $request->file('certIncorp'); 
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($upload) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
            $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
                // ->where('filename', '=', 'Seplat Vendor Data')
                // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Certificate of Incorpoation or Registration')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Certificate of Incorpoation or Registration');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Certificate of Incorpoation or Registration')
            ->first(); // There could be duplicate directory names!
        foreach ($upload as $file) {
            // $filer = $request->file('myFile');
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// Form CO2&CO7 or Form 2.3 & 2.5
Route::post('/formCOTWO', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $upload = $request->file('formCOTWO'); 
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($upload) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
            $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
                // ->where('filename', '=', 'Seplat Vendor Data')
                // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Form CO2&CO7 or Form 2.3 & 2.5')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Form CO2&CO7 or Form 2.3 & 2.5');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Form CO2&CO7 or Form 2.3 & 2.5')
            ->first(); // There could be duplicate directory names!
        foreach ($upload as $file) {
            // $filer = $request->file('myFile');
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// Memorandum & Articles of Association
Route::post('/memorandum', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $upload = $request->file('memorandum'); 
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($upload) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
            $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
                // ->where('filename', '=', 'Seplat Vendor Data')
                // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Memorandum & Articles of Association')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Memorandum & Articles of Association');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Memorandum & Articles of Association')
            ->first(); // There could be duplicate directory names!
        foreach ($upload as $file) {
            // $filer = $request->file('myFile');
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// Proof of Tax Identification Number (TIN)
Route::post('/proofTax', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $upload = $request->file('proofTax'); 
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($upload) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
            $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
                // ->where('filename', '=', 'Seplat Vendor Data')
                // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Proof of Tax Identification Number (TIN)')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Proof of Tax Identification Number (TIN)');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Proof of Tax Identification Number (TIN)')
            ->first(); // There could be duplicate directory names!
        foreach ($upload as $file) {
            // $filer = $request->file('myFile');
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// Income tax clearance for the past 3 years
Route::post('/incomeTax', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $upload = $request->file('incomeTax'); 
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($upload) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
            $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
                // ->where('filename', '=', 'Seplat Vendor Data')
                // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Income tax clearance for the past 3 years')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Income tax clearance for the past 3 years');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Income tax clearance for the past 3 years')
            ->first(); // There could be duplicate directory names!
        foreach ($upload as $file) {
            // $filer = $request->file('myFile');
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// Company profile showing Org Chart/CV of Key personnel
Route::post('/companyProfile', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $upload = $request->file('companyProfile'); 
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($upload) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
            $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
                // ->where('filename', '=', 'Seplat Vendor Data')
                // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Company profile showing Org Chart or CV of Key personnel')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Company profile showing Org Chart or CV of Key personnel');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Company profile showing Org Chart or CV of Key personnel')
            ->first(); // There could be duplicate directory names!
        foreach ($upload as $file) {
            // $filer = $request->file('myFile');
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

//  Proof of Medical Retainership
Route::post('/proofMedical', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $upload = $request->file('proofMedical'); 
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($upload) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
            $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
                // ->where('filename', '=', 'Seplat Vendor Data')
                // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Proof of Medical Retainership')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Proof of Medical Retainership');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Proof of Medical Retainership')
            ->first(); // There could be duplicate directory names!
        foreach ($upload as $file) {
            // $filer = $request->file('myFile');
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// Bank Reference Letter
Route::post('/bankRef', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $bankRef = $request->file('bankRef');
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($bankRef) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
             $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Bank Reference Letter')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Bank Reference Letter');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
        // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Bank Reference Letter')
            ->first(); // There could be duplicate directory names!
        
        foreach ($bankRef as $file) {
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// Last Audited accounts or Statement of affairs
Route::post('/lastAudit', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $lastAudit = $request->file('lastAudit');
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // $lastAudit = $request->file('lastAudit');
    if ($lastAudit) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
            $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')word
            // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Last Audited accounts or Statement of affairs')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Last Audited accounts or Statement of affairs');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Last Audited accounts or Statement of affairs')
            ->first(); // There could be duplicate directory names!
        foreach ($lastAudit as $file) {
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// Bank details on official letterhead, duly signed
Route::post('/bankDetails', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $bankDetails = $request->file('bankDetails');
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($bankDetails) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
             $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Bank details on official letterhead, duly signed')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Bank details on official letterhead, duly signed');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Bank details on official letterhead, duly signed')
            ->first(); // There could be duplicate directory names!
        
        foreach ($bankDetails as $file) {
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// DPR Permit
Route::post('/dprPermit', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $bankDetails = $request->file('dprPermit');
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($bankDetails) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
             $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'DPR Permit')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/DPR Permit');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'DPR Permit')
            ->first(); // There could be duplicate directory names!
        
        foreach ($bankDetails as $file) {
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// Introduction Letter from Community
Route::post('/introLetter', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $bankDetails = $request->file('introLetter');
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($bankDetails) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
             $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Introduction Letter from Community')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Introduction Letter from Community');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Introduction Letter from Community')
            ->first(); // There could be duplicate directory names!
        
        foreach ($bankDetails as $file) {
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// Current workmen compensation or NSITF Registration
Route::post('/workmen', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $bankDetails = $request->file('workmen');
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($bankDetails) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
             $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Current workmen compensation or NSITF Registration')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Current workmen compensation or NSITF Registration');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Current workmen compensation or NSITF Registration')
            ->first(); // There could be duplicate directory names!
        
        foreach ($bankDetails as $file) {
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// General 3rd party insurance coverage
Route::post('/thirdParty', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $bankDetails = $request->file('thirdParty');
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($bankDetails) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
             $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'General 3rd party insurance coverage')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/General 3rd party insurance coverage');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'General 3rd party insurance coverage')
            ->first(); // There could be duplicate directory names!
        
        foreach ($bankDetails as $file) {
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// HSSE Policy
Route::post('/hssePolicy', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $bankDetails = $request->file('hssePolicy');
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($bankDetails) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
             $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'HSSE Policy')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/HSSE Policy');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'HSSE Policy')
            ->first(); // There could be duplicate directory names!
        
        foreach ($bankDetails as $file) {
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// Quality Assurance / Quality Control Policy
Route::post('/assurance', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $bankDetails = $request->file('assurance');
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($bankDetails) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
             $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'Quality Assurance or Quality Control Policy')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/Quality Assurance or Quality Control Policy');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'Quality Assurance or Quality Control Policy')
            ->first(); // There could be duplicate directory names!
        
        foreach ($bankDetails as $file) {
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});

// OEM Agency letter (if applicable)
Route::post('/agency', function(Request $request) {
    set_time_limit(600);
    $vendorName = $request->input('vendor');
    $bankDetails = $request->file('agency');
    // Find parent dir for reference
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
    // checking if the folder Seplat Vendor Data Exist
    $dir = $contents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!

    // if the directory doesnt exist then we create it
    if (!$dir) {
        Storage::cloud()->makeDirectory('Seplat Vendor Data');
    }

    $dir = '/';
    $vendorRecursive = false; // Get subdirectories also?
    $vendorContents = collect(Storage::cloud()->listContents($dir, $vendorRecursive));
    $createVendorFolder = $vendorContents->where('type', '=', 'dir')
        ->where('filename', '=', 'Seplat Vendor Data')
        ->first(); // There could be duplicate directory names!
    if ($createVendorFolder) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));
        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if (!$cvrfDir) {
            Storage::cloud()->makeDirectory($createVendorFolder['path'].'/'.$vendorName);
        }
    } 
    // upload file
    if ($bankDetails) {
        $dir = '/';
        $cvrfRecursive = true; // Get subdirectories also?
        $cvrfContents = collect(Storage::cloud()->listContents($dir, $cvrfRecursive));

        $cvrfDir = $cvrfContents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            ->where('filename', '=', $vendorName)
            ->first(); // There could be duplicate directory names!
        if ($cvrfDir) {
             $dir = '/';
            $recursive = true; // Get subdirectories also?
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
                ->where('filename', '=', 'OEM Agency letter (if applicable)')
                ->first(); // There could be duplicate directory names!
            if (!$child) {
                Storage::cloud()->makeDirectory($cvrfDir['path'].'/OEM Agency letter (if applicable)');                
            }
        }
        $dir = '/';
        $recursive = true; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));

        $child = $contents->where('type', '=', 'dir')
            // ->where('filename', '=', 'Seplat Vendor Data')
            // ->where('filename', '=', $vendorName)
            ->where('filename', '=', 'OEM Agency letter (if applicable)')
            ->first(); // There could be duplicate directory names!
        
        foreach ($bankDetails as $file) {
            $filename = Storage::disk('public')->put('/', $file);
            $filePath = public_path($filename);
            // Storage::cloud()->put($dir['path'].'/'.$filename, fopen($filePath, 'r+'));
            if(Storage::cloud()->put($child['path'].'/'.$filename, fopen($filePath, 'r+')) && File::exists($filePath)) {
                File::delete($filePath);
            }
        }
    } else {
        return "failed";
    }
    set_time_limit(30);
    return "success";
});






















































