import React, { Component } from 'react';
import Header from "../components/header/header";
import Vendorcategories from '../components/vendorcategories/vendorcategories';
import Info from '../components/info/info';
import Upload from '../components/upload/upload';

class Company extends Component {
  render() {
    return (
      <div>
        <Header />
        <div style={{ marginTop: 70 }} ></div>
        <Info name={this.props.location.state.vendor.name} email={this.props.location.state.vendor.email} />
        <br/>
        <Vendorcategories base={"http://127.0.0.1:8000"} vendorId={this.props.location.state.vendor.id} />
        <br/>
        <Upload base={"http://127.0.0.1:8000"} 
          name={this.props.location.state.vendor.name} 
          vendorId={this.props.location.state.vendor.id} 
          vendorEmail={this.props.location.state.vendor.email}
        />
      </div>
    );
  }
}

export default Company;