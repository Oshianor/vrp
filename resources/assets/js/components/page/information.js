import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Header from "../components/header/header";
import { Container } from "reactstrap";

class Information extends Component {
  render() {
    return (
      <div>
        <Header />
        <Container style={{ marginTop: 75 }} >
          <h2>Instructions to Vendors</h2>
          <p>All Vendors are required to access the Seplat Vendor Revalidation portal at this <Link to="/">Link</Link>. <br />
            Click on the Download tab to access the three required forms. <br />
            Click on download next to each form to download to your computer system. <br />
            Complete each form and print/sign as necessary. <br />
            Scan file and save with relevant name including company name. e.g. Vendor Registration From_ABC Company</p>
          <ul>
            <li><a href="/download" download="Vendor Registration Form">Vendor Registration Form</a></li>
            {/* <li>Seplat Contractor HSE Capability Form</li>
            <li>Seplat Vendor Category Selection Form</li> */}
          </ul>
          

          <p>Access the Seplat Vendor Revalidation portal at this Link.
          Enter the unique six-digit number in the box labeled <em>“ Enter Vendor Code ”</em></p>
          <strong>NOTE:</strong> If you have not received your number please send an email to: &nbsp; 
          <a href="mailto:vendorrevalidation@seplatpetroleum.com">vendorrevalidation@seplatpetroleum.com</a>. <br /><br/>

          Select your vendor category from the drop down box and select Enter.  See options below;
          <ul>
            <li>International – All companies operating outside of Nigeria</li>
            <li>Community  - All vendors nominated by the local communities in which Seplat operates. Vendors in this category must have a letter of introduction from a local community.</li>
            <li>Local – All public and private companies registered and operating in Nigeria</li>
            <li>Non Profit – All Non Profit or NGOs only</li>
            <li>Government Agencies– All government agencies, ministries etc. </li>
            <li>Professional Bodies – All registered organizations that act as professional bodies</li>
          </ul>

          
          Select the check boxes for all documents to be uploaded.<br/>
          Click the choose file button.  <br/>
          Find the file on your system to be uploaded and click select.<br/>
          When you have selected all the files to be uploaded click on SUBMIT at the bottom of the page to upload to our site.<br/>
          Our team will contact you via email to confirm submission status.  <br/>
          If additional documents are required you will be notified and must submit all updates before the deadline.  <br/>
          If you do not receive a response within 3 business days please send an email to: &nbsp; <a href="mailto:vendorrevalidation@seplatpetroleum.com">vendorrevalidation@seplatpetroleum.com</a>. <br/>
          <strong>NOTE:</strong> If you need to return to the site to add additional documents or amend previous submissions do so using the same steps.
        </Container>
      </div>
    );
  }
}

export default Information;