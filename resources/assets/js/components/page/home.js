import React, { Component } from 'react';
import Homer from "../components/home/homer";
import Header from "../components/header/header";

class Home extends Component {
  render() {
    return (
      <div>
        <Header />
        <Homer base={"http://127.0.0.1:8000"} />
      </div>
    );
  }
}

export default Home;