export const companytype = (kind) => {
  return {
    type: "COMPANY_TYPE",
    payload: kind
  }
}