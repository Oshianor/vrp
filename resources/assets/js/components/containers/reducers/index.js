const initialstate = {
  ctype: "",
}
export default (state = initialstate, action) => {
  switch (action.type) {
    case "COMPANY_TYPE":
      return Object.assign({}, state, {
        ctype: action.payload,
      });
    case "NAVIGATION":
      return Object.assign({}, state, {
        nav: action.payload
      });
    default:
      return state;
  }
}