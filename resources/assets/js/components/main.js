import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import 'typeface-roboto';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import appReducer from './containers/reducers/index';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "./page/home";
import 'bootstrap/dist/css/bootstrap.min.css';
import Company from "./page/company";
import Information from "./page/information";
import 'font-awesome/css/font-awesome.min.css';

const store = createStore(appReducer, applyMiddleware(thunk));

export default class Main extends Component {
	render() {
		return (
			<BrowserRouter>
				<Switch>
					<Route exact path="/" component={Home} />
					 <Route exact path="/information" component={Information} />
					<Route exact path="/company/:id" component={Company} />
				</Switch>
			</BrowserRouter>
		);
	}
}

if (document.getElementById('app')) {
    ReactDOM.render(
			<Provider store={store}>
				<Main />
			</Provider>
		, document.getElementById('app'));
}
store.subscribe(() => {
	console.log("Store Changed, ", store.getState());
});