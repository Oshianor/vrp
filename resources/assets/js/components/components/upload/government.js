import React, { Component } from 'react';
import { Form, FormGroup, Label, Button, Input } from 'reactstrap';
import axios from 'axios';
import ReactDOM from "react-dom";
import PreloaderIcon from 'react-preloader-icon';
import Spinning from 'react-preloader-icon/loaders/Spinning';
import Downreg from './downreg';
import Alert from './modal';

class Private extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cvrfStatus: "",
      bankRefStatus: "",
      bankDetailsStatus: "",
      lastAuditStatus: "",
      cvrf: "No",
      bankRef: "No",
      bankDetails: "No",
      lastAudit: "No",
      modal: false,
      completed: false
    }
  }

  handleSubmit = () => {
    let cvrf = ReactDOM.findDOMNode(this.refs.cvrf).files;
    if (typeof cvrf[0] !== "undefined") {
      const cvrfData = new FormData();
      cvrfData.append('vendor', this.props.name);
      for (var i = 0; i < cvrf.length; i++) {
        let file = cvrf[i];
        cvrfData.append('cvrf[' + i + ']', file);
      }
      this.setState({
        cvrfStatus: "waiting",
        cvrf: "Yes"
      })
      axios.post(this.props.base + "/cvrf", cvrfData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }).then(res => {
          if (res.status === 200) {
            ReactDOM.findDOMNode(this.refs.cvrf).value = "";
            this.setState({
              cvrfStatus: "completed"
            })
          } else {
            ReactDOM.findDOMNode(this.refs.cvrf).value = "";
            this.setState({
              cvrfStatus: "failed",
              cvrf: []
            })
          }
        });
    } else {
      this.setState({
        cvrfStatus: "completed"
      })
    }
  }

  componentDidUpdate(prevProp, prevState) {
    if (prevState.cvrfStatus !== this.state.cvrfStatus) {
      if (this.state.cvrfStatus === "completed") {
        let bankRef = ReactDOM.findDOMNode(this.refs.bankRef).files;
        if (typeof bankRef[0] !== "undefined") {
          const bankRefData = new FormData();
          bankRefData.append('vendor', this.props.name);
          for (var i = 0; i < bankRef.length; i++) {
            let file = bankRef[i];
            bankRefData.append('bankRef[' + i + ']', file);
          }
          this.setState({
            bankRefStatus: "waiting",
            bankRef: "Yes"
          })
          axios.post(this.props.base + "/bankRef", bankRefData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.bankRef).value = "";
                this.setState({
                  bankRefStatus: "completed"
                })
              } else {
                this.setState({
                  bankRefStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            bankRefStatus: "completed"
          })
        }

      }
    }


    if (prevState.bankRefStatus !== this.state.bankRefStatus) {
      if (this.state.bankRefStatus === "completed") {
        let bankDetails = ReactDOM.findDOMNode(this.refs.bankDetails).files;
        if (typeof bankDetails[0] !== "undefined") {
          const bankDetailsData = new FormData();
          bankDetailsData.append('vendor', this.props.name);
          for (var i = 0; i < bankDetails.length; i++) {
            let file = bankDetails[i];
            bankDetailsData.append('bankDetails[' + i + ']', file);
          }
          this.setState({
            bankDetailsStatus: "waiting",
            bankDetails: "Yes"
          })
          axios.post(this.props.base + "/bankDetails", bankDetailsData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.bankDetails).value = "";
                this.setState({
                  bankDetailsStatus: "completed"
                })
              } else {
                this.setState({
                  bankDetailsStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            bankDetailsStatus: "completed"
          })
        }

      }
    }


    if (prevState.bankDetailsStatus !== this.state.bankDetailsStatus) {
      if (this.state.bankDetailsStatus === "completed") {
        let lastAudit = ReactDOM.findDOMNode(this.refs.lastAudit).files;
        if (typeof lastAudit[0] !== "undefined") {
          const lastAuditData = new FormData();
          lastAuditData.append('vendor', this.props.name);
          for (var i = 0; i < lastAudit.length; i++) {
            let file = lastAudit[i];
            lastAuditData.append('lastAudit[' + i + ']', file);
          }
          this.setState({
            lastAuditStatus: "waiting",
            lastAudit: "Yes"
          })
          axios.post(this.props.base + "/lastAudit", lastAuditData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.lastAudit).value = "";
                this.setState({
                  lastAuditStatus: "completed",
                  completed: true
                })
              } else {
                this.setState({
                  lastAuditStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            lastAuditStatus: "completed",
            completed: true
          })
        }

      }
    }

    if (prevState.completed !== this.state.completed) {
      if (this.state.completed) {
        const email = new FormData();
        email.append('vendor', this.props.name);
        email.append('vendorEmail', this.props.vendorEmail);
        email.append('type', "Gov");        
        email.append('cvrf', this.state.cvrf);
        email.append('bankRef', this.state.bankRef);
        email.append('bankDetails', this.state.bankDetails);
        email.append('lastAudit', this.state.lastAudit);
        axios.post(this.props.base + "/send/email", email, 
          {
            headers: {
              'Content-Type': 'multipart/form-data'
            },
        });
        
      }
    }
  }

  render() {
    return (
      <Form>
        <FormGroup>
          <Label className="heed">
            1. Completed Vendor Registration Form
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.cvrfStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.cvrfStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.cvrfStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="cvrf" />
          <Downreg />
        </FormGroup>

        <FormGroup>
          <Label className="heed" >
            2. Bank Reference Letter
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.bankRefStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.bankRefStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.bankRefStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="bankRef" />
        </FormGroup>

        <FormGroup>
          <Label for="exampleFile" className="heed" >
            3. Bank details on official letterhead, duly signed
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.bankDetailsStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.bankDetailsStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.bankDetailsStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="bankDetails" id="exampleFile"  />
        </FormGroup>

        <FormGroup>
          <Label for="exampleCustomFileBrowser" className="heed" >
            4. Last Audited accounts / Statement of affairs
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.lastAuditStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.lastAuditStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.lastAuditStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="lastAudit" id="exampleFile" />
        </FormGroup>
        <Alert 
          modal={this.state.completed}
        />

        <Button
          color="danger"
          size="lg"
          onClick={this.handleSubmit}
        >
          Submit
        </Button>
      </Form>
    );
  }
}

export default Private;