import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Redirect } from "react-router";

class Alert extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false
    };

    // this.toggle = this.toggle.bind(this);
  }

  toggle = () => {
    this.setState({
      redirect: true
    });
    // this.props.toggle(!this.props.modal);
  }

  render() {
    return (
      <Modal isOpen={this.props.modal} toggle={this.toggle} className={this.props.className}>
        <ModalHeader toggle={this.toggle}>Thank you for your submission</ModalHeader>
        <ModalBody>
          A confirmation has been sent to your email address provided
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.toggle}>Okay</Button>
        </ModalFooter>
        {
          this.state.redirect &&
            <Redirect to="/" />
        }
      </Modal>
    );
  }
}

export default Alert;