import React, { Component } from 'react';
import { Form, FormGroup, Label, Button, Input } from 'reactstrap';
import axios from 'axios';
import ReactDOM from "react-dom";
import PreloaderIcon from 'react-preloader-icon';
import Spinning from 'react-preloader-icon/loaders/Spinning';
import Downreg from './downreg';
import Alert from './modal';

class Profession extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cvrfStatus: "",
      certIncorpStatus: "",
      formCOTWOStatus: "",
      memorandumStatus: "",
      proofTaxStatus: "",
      incomeTaxStatus: "",
      companyProfileStatus: "",
      proofMedicalStatus: "",
      bankRefStatus: "",
      bankDetailsStatus: "",
      lastAuditStatus: "",
      workmenStatus: "",
      thirdPartyStatus: "",
      hssePolicyStatus: "",
      assuranceStatus: "",
      agencyStatus: "",
      // dfghj
      completed: false,
      cvrf: "No",
      certIncorp: "No",
      formCOTWO: "No",
      memorandum: "No",
      proofTax: "No",
      incomeTax: "No",
      companyProfile: "No",
      proofMedical: "No",
      bankRef: "No",
      bankDetails: "No",
      lastAudit: "No",
      workmen: "No",
      thirdParty: "No",
      hssePolicy: "No",
      assurance: "No",
      agency: "No",
    }
  }

  handleSubmit = () => {
    let cvrf = ReactDOM.findDOMNode(this.refs.cvrf).files;
    if (typeof cvrf[0] !== "undefined") {
      const cvrfData = new FormData();
      cvrfData.append('vendor', this.props.name);
      for (var i = 0; i < cvrf.length; i++) {
        let file = cvrf[i];
        cvrfData.append('cvrf[' + i + ']', file);
      }
      this.setState({
        cvrfStatus: "waiting",
        cvrf: "Yes",
      })
      axios.post(this.props.base + "/cvrf", cvrfData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }).then(res => {
          console.log(res);

          if (res.status === 200) {
            ReactDOM.findDOMNode(this.refs.cvrf).value = "";
            this.setState({
              cvrfStatus: "completed"
            })
          } else {
            ReactDOM.findDOMNode(this.refs.cvrf).value = "";
            this.setState({
              cvrfStatus: "failed",
              cvrf: []
            })
          }
        });
    } else {
      this.setState({
        cvrfStatus: "completed"
      })
    }
  }

  componentDidUpdate(prevProp, prevState) {
    if (prevState.cvrfStatus !== this.state.cvrfStatus) {
      if (this.state.cvrfStatus === "completed") {
        let certIncorp = ReactDOM.findDOMNode(this.refs.certIncorp).files;
        if (typeof certIncorp[0] !== "undefined") {
          const certIncorpData = new FormData();
          certIncorpData.append('vendor', this.props.name);
          for (var i = 0; i < certIncorp.length; i++) {
            let file = certIncorp[i];
            certIncorpData.append('certIncorp[' + i + ']', file);
          }
          this.setState({
            certIncorpStatus: "waiting",
            certIncorp: "Yes",
          })
          axios.post(this.props.base + "/certIncorp", certIncorpData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.certIncorp).value = "";
                this.setState({
                  certIncorpStatus: "completed",
                })
              } else {
                ReactDOM.findDOMNode(this.refs.certIncorp).value = "";
                this.setState({
                  certIncorpStatus: "failed",
                })
              }
            })
        } else {
          this.setState({
            certIncorpStatus: "completed"
          })
        }
      }
    }


    if (prevState.certIncorpStatus !== this.state.certIncorpStatus) {
      let formCOTWO = ReactDOM.findDOMNode(this.refs.formCOTWO).files;
      if (this.state.certIncorpStatus === "completed") {
        if (typeof formCOTWO[0] !== "undefined") {
          const formCOTWOData = new FormData();
          formCOTWOData.append('vendor', this.props.name);
          for (var i = 0; i < formCOTWO.length; i++) {
            let file = formCOTWO[i];
            formCOTWOData.append('formCOTWO[' + i + ']', file);
          }
          this.setState({
            formCOTWOStatus: "waiting",
            formCOTWO: "Yes",
          })
          axios.post(this.props.base + "/formCOTWO", formCOTWOData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.formCOTWO).value = "";
                this.setState({
                  formCOTWOStatus: "completed"
                })
              } else {
                this.setState({
                  formCOTWOStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            formCOTWOStatus: "completed"
          })
        }
      }
    }


    if (prevState.formCOTWOStatus !== this.state.formCOTWOStatus) {
      if (this.state.formCOTWOStatus === "completed") {
        let memorandum = ReactDOM.findDOMNode(this.refs.memorandum).files;
        if (typeof memorandum[0] !== "undefined") {
          const memorandumData = new FormData();
          memorandumData.append('vendor', this.props.name);
          for (var i = 0; i < memorandum.length; i++) {
            let file = memorandum[i];
            memorandumData.append('memorandum[' + i + ']', file);
          }
          this.setState({
            memorandumStatus: "waiting",
            memorandum: "Yes",
          })
          axios.post(this.props.base + "/memorandum", memorandumData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.memorandum).value = "";
                this.setState({
                  memorandumStatus: "completed"
                })
              } else {
                this.setState({
                  memorandumStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            memorandumStatus: "completed"
          })
        }
      }
    }


    if (prevState.memorandumStatus !== this.state.memorandumStatus) {
      if (this.state.memorandumStatus === "completed") {
        let proofTax = ReactDOM.findDOMNode(this.refs.proofTax).files;
        if (typeof proofTax[0] !== "undefined") {
          const proofTaxData = new FormData();
          proofTaxData.append('vendor', this.props.name);
          for (var i = 0; i < proofTax.length; i++) {
            let file = proofTax[i];
            proofTaxData.append('proofTax[' + i + ']', file);
          }
          this.setState({
            proofTaxStatus: "waiting",
            proofTax: "Yes",
          })
          axios.post(this.props.base + "/proofTax", proofTaxData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.proofTax).value = "";
                this.setState({
                  proofTaxStatus: "completed"
                })
              } else {
                this.setState({
                  proofTaxStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            proofTaxStatus: "completed"
          })
        }
      }
    }


    if (prevState.proofTaxStatus !== this.state.proofTaxStatus) {
      if (this.state.proofTaxStatus === "completed") {
        let incomeTax = ReactDOM.findDOMNode(this.refs.incomeTax).files;
        if (typeof incomeTax[0] !== "undefined") {
          const incomeTaxData = new FormData();
          incomeTaxData.append('vendor', this.props.name);
          for (var i = 0; i < incomeTax.length; i++) {
            let file = incomeTax[i];
            incomeTaxData.append('incomeTax[' + i + ']', file);
          }
          this.setState({
            incomeTaxStatus: "waiting",
            incomeTax: "Yes",
          })
          axios.post(this.props.base + "/incomeTax", incomeTaxData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.incomeTax).value = "";
                this.setState({
                  incomeTaxStatus: "completed"
                })
              } else {
                this.setState({
                  incomeTaxStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            incomeTaxStatus: "completed"
          })
        }

      }
    }


    if (prevState.incomeTaxStatus !== this.state.incomeTaxStatus) {
      if (this.state.incomeTaxStatus === "completed") {
        let companyProfile = ReactDOM.findDOMNode(this.refs.companyProfile).files;
        if (typeof companyProfile[0] !== "undefined") {
          const companyProfileData = new FormData();
          companyProfileData.append('vendor', this.props.name);
          for (var i = 0; i < companyProfile.length; i++) {
            let file = companyProfile[i];
            companyProfileData.append('companyProfile[' + i + ']', file);
          }
          this.setState({
            companyProfileStatus: "waiting",
            companyProfile: "Yes",
          })
          axios.post(this.props.base + "/companyProfile", companyProfileData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.companyProfile).value = "";
                this.setState({
                  companyProfileStatus: "completed"
                })
              } else {
                this.setState({
                  companyProfileStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            companyProfileStatus: "completed"
          })
        }

      }
    }


    if (prevState.companyProfileStatus !== this.state.companyProfileStatus) {
      if (this.state.companyProfileStatus === "completed") {
        let proofMedical = ReactDOM.findDOMNode(this.refs.proofTax).files;
        if (typeof proofMedical[0] !== "undefined") {
          const proofMedicalData = new FormData();
          proofMedicalData.append('vendor', this.props.name);
          for (var i = 0; i < proofMedical.length; i++) {
            let file = proofMedical[i];
            proofMedicalData.append('proofMedical[' + i + ']', file);
          }
          this.setState({
            proofMedicalStatus: "waiting",
            proofMedical: "Yes",
          })
          axios.post(this.props.base + "/proofMedical", proofMedicalData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.proofMedical).value = "";
                this.setState({
                  proofMedicalStatus: "completed"
                })
              } else {
                this.setState({
                  proofMedicalStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            proofMedicalStatus: "completed"
          })
        }

      }
    }


    if (prevState.proofMedicalStatus !== this.state.proofMedicalStatus) {
      if (this.state.proofMedicalStatus === "completed") {
        let bankRef = ReactDOM.findDOMNode(this.refs.bankRef).files;
        if (typeof bankRef[0] !== "undefined") {
          const bankRefData = new FormData();
          bankRefData.append('vendor', this.props.name);
          for (var i = 0; i < bankRef.length; i++) {
            let file = bankRef[i];
            bankRefData.append('bankRef[' + i + ']', file);
          }
          this.setState({
            bankRefStatus: "waiting",
            bankRef: "Yes",
          })
          axios.post(this.props.base + "/bankRef", bankRefData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.bankRef).value = "";
                this.setState({
                  bankRefStatus: "completed"
                })
              } else {
                this.setState({
                  bankRefStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            bankRefStatus: "completed"
          })
        }

      }
    }


    if (prevState.bankRefStatus !== this.state.bankRefStatus) {
      if (this.state.bankRefStatus === "completed") {
        let bankDetails = ReactDOM.findDOMNode(this.refs.bankDetails).files;
        if (typeof bankDetails[0] !== "undefined") {
          const bankDetailsData = new FormData();
          bankDetailsData.append('vendor', this.props.name);
          for (var i = 0; i < bankDetails.length; i++) {
            let file = bankDetails[i];
            bankDetailsData.append('bankDetails[' + i + ']', file);
          }
          this.setState({
            bankDetailsStatus: "waiting",
            bankDetails: "Yes",
          })
          axios.post(this.props.base + "/bankDetails", bankDetailsData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.bankDetails).value = "";
                this.setState({
                  bankDetailsStatus: "completed"
                })
              } else {
                this.setState({
                  bankDetailsStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            bankDetailsStatus: "completed"
          })
        }

      }
    }


    if (prevState.bankDetailsStatus !== this.state.bankDetailsStatus) {
      if (this.state.bankDetailsStatus === "completed") {
        let lastAudit = ReactDOM.findDOMNode(this.refs.lastAudit).files;
        if (typeof lastAudit[0] !== "undefined") {
          const lastAuditData = new FormData();
          lastAuditData.append('vendor', this.props.name);
          for (var i = 0; i < lastAudit.length; i++) {
            let file = lastAudit[i];
            lastAuditData.append('lastAudit[' + i + ']', file);
          }
          this.setState({
            lastAuditStatus: "waiting",
            lastAudit: "Yes",
          })
          axios.post(this.props.base + "/lastAudit", lastAuditData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.lastAudit).value = "";
                this.setState({
                  lastAuditStatus: "completed",
                })
              } else {
                this.setState({
                  lastAuditStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            lastAuditStatus: "completed",
          })
        }

      }
    }


    if (prevState.lastAuditStatus !== this.state.lastAuditStatus) {
      if (this.state.lastAuditStatus === "completed") {
        let workmen = ReactDOM.findDOMNode(this.refs.workmen).files;
        if (typeof workmen[0] !== "undefined") {
          const workmenData = new FormData();
          workmenData.append('vendor', this.props.name);
          for (var i = 0; i < workmen.length; i++) {
            let file = workmen[i];
            workmenData.append('workmen[' + i + ']', file);
          }
          this.setState({
            workmenStatus: "waiting",
            workmen: "Yes",
          })
          axios.post(this.props.base + "/workmen", workmenData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.workmen).value = "";
                this.setState({
                  workmenStatus: "completed",
                })
              } else {
                this.setState({
                  workmenStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            workmenStatus: "completed",
          })
        }
      }
    }


    if (prevState.workmenStatus !== this.state.workmenStatus) {
      if (this.state.workmenStatus === "completed") {
        let thirdParty = ReactDOM.findDOMNode(this.refs.thirdParty).files;
        if (typeof thirdParty[0] !== "undefined") {
          const thirdPartyData = new FormData();
          thirdPartyData.append('vendor', this.props.name);
          for (var i = 0; i < thirdParty.length; i++) {
            let file = thirdParty[i];
            thirdPartyData.append('thirdParty[' + i + ']', file);
          }
          this.setState({
            thirdPartyStatus: "waiting",
            thirdParty: "Yes",
          })
          axios.post(this.props.base + "/thirdParty", thirdPartyData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.thirdParty).value = "";
                this.setState({
                  thirdPartyStatus: "completed",
                })
              } else {
                this.setState({
                  thirdPartyStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            thirdPartyStatus: "completed",
          })
        }
      }
    }


    if (prevState.thirdPartyStatus !== this.state.thirdPartyStatus) {
      if (this.state.thirdPartyStatus === "completed") {
        let hssePolicy = ReactDOM.findDOMNode(this.refs.hssePolicy).files;
        if (typeof hssePolicy[0] !== "undefined") {
          const hssePolicyData = new FormData();
          hssePolicyData.append('vendor', this.props.name);
          for (var i = 0; i < hssePolicy.length; i++) {
            let file = lastAudit[i];
            hssePolicyData.append('hssePolicy[' + i + ']', file);
          }
          this.setState({
            hssePolicyStatus: "waiting",
            hssePolicy: "Yes",
          })
          axios.post(this.props.base + "/hssePolicy", hssePolicyData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.hssePolicy).value = "";
                this.setState({
                  hssePolicyStatus: "completed",
                })
              } else {
                this.setState({
                  hssePolicyStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            hssePolicyStatus: "completed",
          })
        }
      }
    }


    if (prevState.hssePolicyStatus !== this.state.hssePolicyStatus) {
      if (this.state.hssePolicyStatus === "completed") {
        let assurance = ReactDOM.findDOMNode(this.refs.assurance).files;
        if (typeof assurance[0] !== "undefined") {
          const assuranceData = new FormData();
          assuranceData.append('vendor', this.props.name);
          for (var i = 0; i < assurance.length; i++) {
            let file = assurance[i];
            assuranceData.append('assurance[' + i + ']', file);
          }
          this.setState({
            assuranceStatus: "waiting",
            assurance: "Yes",
          })
          axios.post(this.props.base + "/assurance", assuranceData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.assurance).value = "";
                this.setState({
                  assuranceStatus: "completed"
                })
              } else {
                this.setState({
                  assuranceStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            assuranceStatus: "completed"
          })
        }
      }
    }


    if (prevState.assuranceStatus !== this.state.assuranceStatus) {
      if (this.state.assuranceStatus === "completed") {
        let agency = ReactDOM.findDOMNode(this.refs.agency).files;
        if (typeof agency[0] !== "undefined") {
          const agencyData = new FormData();
          agencyData.append('vendor', this.props.name);
          for (var i = 0; i < agency.length; i++) {
            let file = agency[i];
            agencyData.append('agency[' + i + ']', file);
          }
          this.setState({
            agencyStatus: "waiting",
            agency: "Yes",
          })
          axios.post(this.props.base + "/agency", agencyData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.agency).value = "";
                this.setState({
                  agencyStatus: "completed",
                  completed: true
                })
              } else {
                this.setState({
                  agencyStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            agencyStatus: "completed",
            completed: true
          })
        }
      }
    }
    
    if (prevState.completed !== this.state.completed) {
      if (this.state.completed) {
        const email = new FormData();
        email.append('vendor', this.props.name);
        email.append('vendorEmail', this.props.vendorEmail);
        email.append('type', "Pro");
        email.append('cvrf', this.state.cvrf);
        email.append('bankRef', this.state.bankRef);
        email.append('bankDetails', this.state.bankDetails);
        email.append('lastAudit', this.state.lastAudit);
        email.append('certIncorp', this.state.certIncorp);
        email.append('formCOTWO', this.state.formCOTWO);
        email.append('memorandum', this.state.memorandum);
        email.append('proofTax', this.state.proofTax);
        email.append('incomeTax', this.state.incomeTax);
        email.append('companyProfile', this.state.companyProfile);
        email.append('proofMedical', this.state.proofMedical);
        email.append('workmen', this.state.workmen);
        email.append('thirdParty', this.state.thirdParty);
        email.append('hssePolicy', this.state.hssePolicy);
        email.append('assurance', this.state.assurance);
        email.append('agency', this.state.agency);
        axios.post(this.props.base + "/send/email", email,
          {
            headers: {
              'Content-Type': 'multipart/form-data'
            },
          });

      }
    }


  }

  render() {
    return (
      <Form>
        <FormGroup>
          <Label className="heed">
            1. Completed Vendor Registration Form
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.cvrfStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.cvrfStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.cvrfStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="cvrf" />
          <Downreg />
        </FormGroup>

        <FormGroup>
          <Label className="heed">
            2. Certificate of Incorpoation / Registration
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.certIncorpStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.certIncorpStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.certIncorpStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="certIncorp" />
        </FormGroup>

        <FormGroup>
          <Label className="heed">
            3. Form CO2&CO7 or Form 2.3 & 2.5
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.formCOTWOStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.formCOTWOStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.formCOTWOStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="formCOTWO" />
        </FormGroup>

        <FormGroup>
          <Label className="heed">
            4. Memorandum & Articles of Association
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.memorandumStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.memorandumStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.memorandumStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="memorandum" />
        </FormGroup>

        <FormGroup>
          <Label className="heed">
            5. Proof of Tax Identification Number (TIN)
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.proofTaxStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.proofTaxStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.proofTaxStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="proofTax" />
        </FormGroup>

        <FormGroup>
          <Label className="heed">
            6. Income tax clearance for the past 3 years
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.incomeTaxStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.incomeTaxStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.incomeTaxStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="incomeTax" />
        </FormGroup>

        <FormGroup>
          <Label className="heed">
            7. Company profile showing Org Chart/CV of Key personnel
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.companyProfileStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.companyProfileStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.companyProfileStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="companyProfile" />
        </FormGroup>

        <FormGroup>
          <Label className="heed">
            8. Proof of Medical Retainership
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.proofMedicalStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.proofMedicalStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.proofMedicalStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="proofMedical" />
        </FormGroup>

        <FormGroup>
          <Label className="heed" >
            9. Bank Reference Letter
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.bankRefStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.bankRefStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.bankRefStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="bankRef" />
        </FormGroup>

        <FormGroup>
          <Label for="exampleFile" className="heed" >
            10. Bank details on official letterhead, duly signed
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.bankDetailsStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.bankDetailsStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.bankDetailsStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="bankDetails" />
        </FormGroup>

        <FormGroup>
          <Label for="exampleCustomFileBrowser" className="heed" >
            11. Last Audited accounts / Statement of affairs
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.lastAuditStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.lastAuditStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.lastAuditStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="lastAudit" />
        </FormGroup>

        <FormGroup>
          <Label for="exampleCustomFileBrowser" className="heed" >
            12. Current workmen compensation or NSITF Registration
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.workmenStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.workmenStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.workmenStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="workmen" />
        </FormGroup>

        <FormGroup>
          <Label for="exampleCustomFileBrowser" className="heed" >
            13. General 3rd party insurance coverage
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.thirdPartyStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.thirdPartyStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.thirdPartyStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="thirdParty" />
        </FormGroup>

        <FormGroup>
          <Label for="exampleCustomFileBrowser" className="heed" >
            14. HSSE Policy
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.hssePolicyStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.hssePolicyStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.hssePolicyStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="hssePolicy" />
        </FormGroup>

        <FormGroup>
          <Label for="exampleCustomFileBrowser" className="heed" >
            15. Quality Assurance / Quality Control Policy
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.assuranceStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.assuranceStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.assuranceStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="assurance" id="exampleFile" />
        </FormGroup>

        <FormGroup>
          <Label for="exampleCustomFileBrowser" className="heed" >
            16. OEM Agency letter (if applicable)
              <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.agencyStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.agencyStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.agencyStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="agency" />
        </FormGroup>

        <Alert
          modal={this.state.completed}
        />

        <Button
          color="danger"
          size="lg"
          onClick={this.handleSubmit}
        >
          Submit
        </Button>
      </Form>
    );
  }
}

export default Profession;