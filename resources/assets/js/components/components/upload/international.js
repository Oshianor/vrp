import React, { Component } from 'react';
import { Form, FormGroup, Label, Button, Input, FormText } from 'reactstrap';
import axios from 'axios';
import ReactDOM from "react-dom";
import PreloaderIcon from 'react-preloader-icon';
import Spinning from 'react-preloader-icon/loaders/Spinning';
import Downreg from './downreg';
import Alert from './modal';

class International extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cvrfStatus: "",
      certIncorpStatus: "",
      proofTaxStatus: "",
      companyProfileStatus: "",
      proofMedicalStatus: "",
      bankDetailsStatus: "",
      lastAuditStatus: "",
      hssePolicyStatus: "",
      assuranceStatus: "",
      agencyStatus: "",

      // dfghjk
      completed: false,
      cvrf: "No",
      certIncorp: "No",
      proofTax: "No",
      companyProfile: "No",
      proofMedical: "No",
      bankDetails: "No",
      lastAudit: "No",
      hssePolicy: "No",
      assurance: "No",
      agency: "No",
    }
  }

  handleSubmit = () => {
    let cvrf = ReactDOM.findDOMNode(this.refs.cvrf).files;
    if (typeof cvrf[0] !== "undefined") {
      const cvrfData = new FormData();
      cvrfData.append('vendor', this.props.name);
      for (var i = 0; i < cvrf.length; i++) {
        let file = cvrf[i];
        cvrfData.append('cvrf[' + i + ']', file);
      }
      this.setState({
        cvrfStatus: "waiting",
        cvrf: "Yes"
      })
      axios.post(this.props.base + "/cvrf", cvrfData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }).then(res => {
          // console.log(res);

          if (res.status === 200) {
            ReactDOM.findDOMNode(this.refs.cvrf).value = "";
            this.setState({
              cvrfStatus: "completed"
            })
          } else {
            ReactDOM.findDOMNode(this.refs.cvrf).value = "";
            this.setState({
              cvrfStatus: "failed",
              cvrf: []
            })
          }
        });
    } else {
      this.setState({
        cvrfStatus: "completed"
      })
    }
  }

  componentDidUpdate(prevProp, prevState) {
    if (prevState.cvrfStatus !== this.state.cvrfStatus) {
      if (this.state.cvrfStatus === "completed") {
        let certIncorp = ReactDOM.findDOMNode(this.refs.certIncorp).files;
        if (typeof certIncorp[0] !== "undefined") {
          const certIncorpData = new FormData();
          certIncorpData.append('vendor', this.props.name);
          for (var i = 0; i < certIncorp.length; i++) {
            let file = certIncorp[i];
            certIncorpData.append('certIncorp[' + i + ']', file);
          }
          this.setState({
            certIncorpStatus: "waiting",
            certIncorp: "Yes",
          })
          axios.post(this.props.base + "/certIncorp", certIncorpData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.certIncorp).value = "";
                this.setState({
                  certIncorpStatus: "completed",
                })
              } else {
                ReactDOM.findDOMNode(this.refs.certIncorp).value = "";
                this.setState({
                  certIncorpStatus: "failed",
                  certIncorp: []
                })
              }
            })
        } else {
          this.setState({
            certIncorpStatus: "completed"
          })
        }
      }
    }



    if (prevState.certIncorpStatus !== this.state.certIncorpStatus) {
      if (this.state.certIncorpStatus === "completed") {
        let proofTax = ReactDOM.findDOMNode(this.refs.proofTax).files;
        if (typeof proofTax[0] !== "undefined") {
          const proofTaxData = new FormData();
          proofTaxData.append('vendor', this.props.name);
          for (var i = 0; i < proofTax.length; i++) {
            let file = proofTax[i];
            proofTaxData.append('proofTax[' + i + ']', file);
          }
          this.setState({
            proofTaxStatus: "waiting",
            proofTax: "Yes",
          })
          axios.post(this.props.base + "/proofTax", proofTaxData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.proofTax).value = "";
                this.setState({
                  proofTaxStatus: "completed"
                })
              } else {
                this.setState({
                  proofTaxStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            proofTaxStatus: "completed"
          })
        }
      }
    }


    if (prevState.proofTaxStatus !== this.state.proofTaxStatus) {
      if (this.state.proofTaxStatus === "completed") {
        let companyProfile = ReactDOM.findDOMNode(this.refs.companyProfile).files;
        if (typeof companyProfile[0] !== "undefined") {
          const companyProfileData = new FormData();
          companyProfileData.append('vendor', this.props.name);
          for (var i = 0; i < companyProfile.length; i++) {
            let file = companyProfile[i];
            companyProfileData.append('companyProfile[' + i + ']', file);
          }
          this.setState({
            companyProfileStatus: "waiting",
            companyProfile: "Yes",
          })
          axios.post(this.props.base + "/companyProfile", companyProfileData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.companyProfile).value = "";
                this.setState({
                  companyProfileStatus: "completed"
                })
              } else {
                this.setState({
                  companyProfileStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            companyProfileStatus: "completed"
          })
        }

      }
    }



    if (prevState.companyProfileStatus !== this.state.companyProfileStatus) {
      if (this.state.companyProfileStatus === "completed") {
        let proofMedical = ReactDOM.findDOMNode(this.refs.proofTax).files;
        if (typeof proofMedical[0] !== "undefined") {
          const proofMedicalData = new FormData();
          proofMedicalData.append('vendor', this.props.name);
          for (var i = 0; i < proofMedical.length; i++) {
            let file = proofMedical[i];
            proofMedicalData.append('proofMedical[' + i + ']', file);
          }
          this.setState({
            proofMedicalStatus: "waiting",
            proofMedical: "Yes",
          })
          axios.post(this.props.base + "/proofMedical", proofMedicalData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.proofMedical).value = "";
                this.setState({
                  proofMedicalStatus: "completed"
                })
              } else {
                this.setState({
                  proofMedicalStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            proofMedicalStatus: "completed"
          })
        }

      }
    }



    if (prevState.proofMedicalStatus !== this.state.proofMedicalStatus) {
      if (this.state.proofMedicalStatus === "completed") {
        let bankDetails = ReactDOM.findDOMNode(this.refs.bankDetails).files;
        if (typeof bankDetails[0] !== "undefined") {
          const bankDetailsData = new FormData();
          bankDetailsData.append('vendor', this.props.name);
          for (var i = 0; i < bankDetails.length; i++) {
            let file = bankDetails[i];
            bankDetailsData.append('bankDetails[' + i + ']', file);
          }
          this.setState({
            bankDetailsStatus: "waiting",
            bankDetails: "Yes",
          })
          axios.post(this.props.base + "/bankDetails", bankDetailsData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.bankDetails).value = "";
                this.setState({
                  bankDetailsStatus: "completed"
                })
              } else {
                this.setState({
                  bankDetailsStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            bankDetailsStatus: "completed"
          })
        }

      }
    }


    if (prevState.bankDetailsStatus !== this.state.bankDetailsStatus) {
      if (this.state.bankDetailsStatus === "completed") {
        let lastAudit = ReactDOM.findDOMNode(this.refs.lastAudit).files;
        if (typeof lastAudit[0] !== "undefined") {
          const lastAuditData = new FormData();
          lastAuditData.append('vendor', this.props.name);
          for (var i = 0; i < lastAudit.length; i++) {
            let file = lastAudit[i];
            lastAuditData.append('lastAudit[' + i + ']', file);
          }
          this.setState({
            lastAuditStatus: "waiting",
            lastAudit: "Yes",
          })
          axios.post(this.props.base + "/lastAudit", lastAuditData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.lastAudit).value = "";
                this.setState({
                  lastAuditStatus: "completed",
                })
              } else {
                this.setState({
                  lastAuditStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            lastAuditStatus: "completed",
          })
        }

      }
    }


    if (prevState.lastAuditStatus !== this.state.lastAuditStatus) {
      if (this.state.lastAuditStatus === "completed") {
        let hssePolicy = ReactDOM.findDOMNode(this.refs.hssePolicy).files;
        if (typeof hssePolicy[0] !== "undefined") {
          const hssePolicyData = new FormData();
          hssePolicyData.append('vendor', this.props.name);
          for (var i = 0; i < hssePolicy.length; i++) {
            let file = lastAudit[i];
            hssePolicyData.append('hssePolicy[' + i + ']', file);
          }
          this.setState({
            hssePolicyStatus: "waiting",
            hssePolicy: "Yes",
          })
          axios.post(this.props.base + "/hssePolicy", hssePolicyData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.hssePolicy).value = "";
                this.setState({
                  hssePolicyStatus: "completed",
                })
              } else {
                this.setState({
                  hssePolicyStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            hssePolicyStatus: "completed",
          })
        }
      }
    }



    if (prevState.hssePolicyStatus !== this.state.hssePolicyStatus) {
      if (this.state.hssePolicyStatus === "completed") {
        let assurance = ReactDOM.findDOMNode(this.refs.assurance).files;
        if (typeof assurance[0] !== "undefined") {
          const assuranceData = new FormData();
          assuranceData.append('vendor', this.props.name);
          for (var i = 0; i < assurance.length; i++) {
            let file = assurance[i];
            assuranceData.append('assurance[' + i + ']', file);
          }
          this.setState({
            assuranceStatus: "waiting",
            assurance: "Yes",
          })
          axios.post(this.props.base + "/assurance", assuranceData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.assurance).value = "";
                this.setState({
                  assuranceStatus: "completed"
                })
              } else {
                this.setState({
                  assuranceStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            assuranceStatus: "completed"
          })
        }
      }
    }



    if (prevState.assuranceStatus !== this.state.assuranceStatus) {
      if (this.state.assuranceStatus === "completed") {
        let agency = ReactDOM.findDOMNode(this.refs.agency).files;
        if (typeof agency[0] !== "undefined") {
          const agencyData = new FormData();
          agencyData.append('vendor', this.props.name);
          for (var i = 0; i < agency.length; i++) {
            let file = agency[i];
            agencyData.append('agency[' + i + ']', file);
          }
          this.setState({
            agencyStatus: "waiting",
            agency: "Yes",
          })
          axios.post(this.props.base + "/agency", agencyData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.agency).value = "";
                this.setState({
                  agencyStatus: "completed",
                  completed: true
                })
              } else {
                this.setState({
                  agencyStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            agencyStatus: "completed",
            completed: true
          })
        }
      }
    }

    if (prevState.completed !== this.state.completed) {
      if (this.state.completed) {
        const email = new FormData();
        email.append('vendor', this.props.name);
        email.append('vendorEmail', this.props.vendorEmail);
        email.append('type', "Inter");
        email.append('cvrf', this.state.cvrf);
        email.append('bankDetails', this.state.bankDetails);
        email.append('lastAudit', this.state.lastAudit);
        email.append('certIncorp', this.state.certIncorp);
        email.append('proofTax', this.state.proofTax);
        email.append('companyProfile', this.state.companyProfile);
        email.append('proofMedical', this.state.proofMedical)
        email.append('hssePolicy', this.state.hssePolicy);
        email.append('assurance', this.state.assurance);
        email.append('agency', this.state.agency);
        axios.post(this.props.base + "/send/email", email,
          {
            headers: {
              'Content-Type': 'multipart/form-data'
            },
          });

      }
    }


  }


  render() {
    return (
      <Form>
        <FormGroup>
          <Label className="heed">
            1. Completed Vendor Registration Form
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.cvrfStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.cvrfStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.cvrfStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="cvrf" />
          <Downreg />
        </FormGroup>

        <FormGroup>
          <Label className="heed">
            2. Certificate of Incorpoation / Registration
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.certIncorpStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.certIncorpStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.certIncorpStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="certIncorp"  />
        </FormGroup>

        <FormGroup>
          <Label className="heed">
            3. Proof of Tax Identification Number (TIN)
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.proofTaxStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.proofTaxStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.proofTaxStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="proofTax" />
          <FormText style={{ color: "red" }} >If registered for taxes in Nigeria</FormText>
        </FormGroup>

        <FormGroup>
          <Label className="heed">
            4. Company profile showing Org Chart/CV of Key personnel
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.companyProfileStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.companyProfileStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.companyProfileStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="companyProfile" />
        </FormGroup>

        <FormGroup>
          <Label className="heed">
            5. Proof of Medical Retainership
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.proofMedicalStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.proofMedicalStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.proofMedicalStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="proofMedical" />
        </FormGroup>

        <FormGroup>
          <Label for="exampleFile" className="heed" >
            6. Bank details on official letterhead, duly signed
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.bankDetailsStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.bankDetailsStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.bankDetailsStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="bankDetails" id="exampleFile" />
        </FormGroup>

        <FormGroup>
          <Label for="exampleCustomFileBrowser" className="heed" >
            7. Last Audited accounts / Statement of affairs
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.lastAuditStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.lastAuditStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.lastAuditStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="lastAudit" id="exampleFile" />
        </FormGroup>

        <FormGroup>
          <Label for="exampleCustomFileBrowser" className="heed" >
            8. HSSE Policy
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.hssePolicyStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.hssePolicyStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.hssePolicyStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="hssePolicy" id="exampleFile" />
        </FormGroup>

        <FormGroup>
          <Label for="exampleCustomFileBrowser" className="heed" >
            9. Quality Assurance / Quality Control Policy
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.assuranceStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.assuranceStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.assuranceStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="assurance" id="exampleFile" />
        </FormGroup>

        <FormGroup>
          <Label for="exampleCustomFileBrowser" className="heed" >
            10. OEM Agency letter (if applicable)
            <span style={{ float: "right", paddingLeft: 10 }} >
              {
                this.state.agencyStatus === "waiting" &&
                <PreloaderIcon
                  loader={Spinning}
                  size={31}
                  strokeWidth={8}
                  strokeColor="red"
                  duration={800}
                />
              }
              {
                this.state.agencyStatus === "completed" &&
                <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
              }
              {
                this.state.agencyStatus === "failed" &&
                <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
              }
            </span>
          </Label>
          <Input type="file" multiple ref="agency" id="exampleFile" />
        </FormGroup>

        <Alert
          modal={this.state.completed}
        />

        <Button
          color="danger"
          size="lg"
          onClick={this.handleSubmit}
        >
          Submit
        </Button>
      </Form>
    );
  }
}

export default International;