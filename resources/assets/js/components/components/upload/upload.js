import React, { Component } from 'react';
import { Container } from 'reactstrap';
import { companytype } from "../../containers/actions/index";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Profession from './profession';
import Ngo from './ngo';
import Government from './government';
import International from './international';
import Private from './private';
import Community from './community';

class Upload extends Component {
  render() {
    return (
      <Container>
        {
          this.props.state.ctype !== "" &&
          <div>
            <h2>Checklist of Required Documents</h2>
            <p>Below is a list of all required documents based on your selected vendor category</p>
          </div>
        }
        {
          this.props.state.ctype === "Inter" &&
          <International
            base={this.props.base} 
            name={this.props.name}  
            vendorId={this.props.vendorId}
            vendorEmail={this.props.vendorEmail} 
          />
        }
        {
          this.props.state.ctype === "Ngo" &&
          <Ngo 
            base={this.props.base} 
            name={this.props.name}  
            vendorId={this.props.vendorId}
            vendorEmail={this.props.vendorEmail} 
          />
        }
        {
          this.props.state.ctype === "Gov" &&
          <Government 
            base={this.props.base} 
            name={this.props.name}  
            vendorId={this.props.vendorId}
            vendorEmail={this.props.vendorEmail} 
          />
        }
        {
          this.props.state.ctype === "Pri" &&
          <Private 
            base={this.props.base} 
            name={this.props.name}  
            vendorId={this.props.vendorId}
            vendorEmail={this.props.vendorEmail} 
          />
        }
        {
          this.props.state.ctype === "Com" &&
          <Community 
            base={this.props.base} 
            name={this.props.name}  
            vendorId={this.props.vendorId}
            vendorEmail={this.props.vendorEmail} 
          />
        }
        {
          this.props.state.ctype === "Pro" &&
          <Profession 
            base={this.props.base} 
            name={this.props.name}  
            vendorId={this.props.vendorId}
            vendorEmail={this.props.vendorEmail} 
          />
        }
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    state: state
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    companytype: companytype,
  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Upload);