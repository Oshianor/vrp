import React, { Component } from 'react';
import { FormText } from 'reactstrap';

class Downreg extends Component {
  render() {
    return (
      <FormText>
        Download <a href="/download" download="Vendor Registration Form">Vendor Registration Form</a>
      </FormText>
    );
  }
}

export default Downreg;