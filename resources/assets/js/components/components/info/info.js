import React, { Component } from 'react';
import Companyname from './components/companyname';
import { Container, FormGroup, Label, Input, Row, Col } from "reactstrap";
import { companytype } from "../../containers/actions/index";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class Info extends Component {
  handleType = (e) => {
    this.props.companytype(e.target.value); 
  }
  render() {
    return (
      <Container>
        <Companyname name={this.props.name} email={this.props.email} />
        <Row>
          <Col xs={12} sm={10} md={8} lg={6} xl={6} >
            <FormGroup>
              <Label for="exampleSelect">Select your company type from the dropdown below</Label>
              <Input type="select" onChange={this.handleType} name="select" id="exampleSelect">
                <option value="" >Select Company Type</option>
                <option value="Inter" >International</option>
                <option value="Pri" >Local - Private</option>
                <option value="Com" >Local - Community</option>
                <option value="Ngo" >Local - NGO</option>
                <option value="Gov" >Local - Government Agency / Parastatal</option>
                <option value="Pro" >Local - Professional Bodies</option>
              </Input>
            </FormGroup>
          </Col>
        </Row>
      </Container>
    );
  }
}
function mapStateToProps(state) {
  return {
    state: state
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    companytype: companytype,
  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Info);