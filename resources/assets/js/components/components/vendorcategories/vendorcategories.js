import React, { Component } from 'react';
import { Table, Container, FormText } from 'reactstrap';
import Dpr from './component/dpr';
import Seplat from './component/seplat';
import Subcat from './component/subcat';

class Vendorcategories extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      selectedDpr: [],
      currentDprRef: "",
      currentSeplat: ""
    }
  }
  
  handleAddDpr = (data) => {
    // addding dpr anme and ref number
    
    let inc = []
    this.state.selectedDpr.forEach(element => {
      inc = inc.concat(element.value);
    });
    if (!inc.includes(data.value) && data.value !== "") {
      this.setState({
        selectedDpr: this.state.selectedDpr.concat(data),
        currentDprRef: data.value,
        currentSeplat: ""
      })
    }
  }

  handleDeleteDpr = (id) => {
    this.state.selectedDpr.splice(id, 1);
    this.setState({
      selectedDpr: this.state.selectedDpr,
      currentDprRef: "",
      currentSeplat: ""
    })
  }

  handleSetCurentDpr = (ref) => {
    // set current ref id of dpr name
    this.setState({
      currentDprRef: ref,
      currentSeplat: ""
    })
  }

  handleSetSeplat = (sep) => {
    this.setState({
      currentSeplat: sep
    })
  }

  pushTo = (sm) => {
    // push the category fetch form the 
    // database to the state selectDpr
    this.setState({
      selectedDpr: sm,
      currentDprRef: sm[0].value
    })
  }
  
  
  render() {
    // console.log("VEndor", this.state);
    
    return (
      <Container>
        <h4>Product / Service Categories</h4>
        <FormText>
            Start by selecting a Main category,
            select check boxes for all appropriate Seplat Categories
        </FormText>
        <Table bordered>
          <thead>
            <tr>
              <th width="50%" >MAIN CATEGORY (DPR CATEGORY)</th>
              <th width="35%" >SEPLAT CATEGORY</th>
              <th width="15%" >SUB-CATEGORY</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <Dpr 
                  base={this.props.base} 
                  selectedDpr={this.state.selectedDpr} 
                  addDpr={this.handleAddDpr.bind(this)}
                  setCurrentDprRef={this.handleSetCurentDpr.bind(this)}
                  currentDprRef={this.state.currentDprRef}
                  deleteDpr={this.handleDeleteDpr.bind(this)}
                  vendorId={this.props.vendorId}
                  pushTo={this.pushTo.bind(this)}
                />
              </td>
              <td>
                <Seplat
                  base={this.props.base} 
                  currentDprRef={this.state.currentDprRef} 
                  currentSeplat={this.state.currentSeplat}
                  setCurrentSeplat={this.handleSetSeplat.bind(this)}
                  vendorId={this.props.vendorId}
                />
              </td>
              <td>
                <Subcat 
                  base={this.props.base}
                  currentSeplat={this.state.currentSeplat}
                />
              </td>
            </tr>
          </tbody>
        </Table>
      </Container>
    );
  }
}

export default Vendorcategories;