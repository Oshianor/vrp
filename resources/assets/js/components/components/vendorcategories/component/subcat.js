import React, { Component } from 'react';
import axios from 'axios';

class Subcat extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      seplat: "", 
      loading: true,
      subcat: []
    }
  }

  componentDidUpdate() {
    if (this.state.seplat !== this.props.currentSeplat) {
      this.setState({
        seplat: this.props.currentSeplat,
        loading: true
      })
      if (this.props.currentSeplat !== "") {
        axios.get(this.props.base + '/api/subcat/' + this.props.currentSeplat)
          .then(res => {
            const subcat = res.data;
            this.setState({
              subcat,
              loading: false
            });
          })
      }
    }
  }

  render() {
    let syl = {
      marginLeft: -20,
      paddingBottom: 15,
    }
    if (this.state.loading) {
      if (this.state.seplat === "") {
        return (
          <div>
            SELECT A SEPLAT
          </div>
        )
      } else {
        return null;
        // (
      //     <Container style={{ width: 250 }} >
      //       <PreloaderIcon
      //         loader={Oval}
      //         size={60}
      //         strokeWidth={8}
      //         strokeColor="#006064"
      //         duration={800}
      //       />
      //       <p style={{ marginLeft: -10 }} >Loading sub-category.</p>
      //     </Container>
      //   );
      }
    } else {
      return (
        <div>
          <ul style={{ listStyleType: "none" }}>
            {
              this.state.subcat.map((sep, index) => (
                <li key={index} style={syl} >
                  <span style={{ marginLeft: -15 }} >{sep.name}</span>
                </li>
              ))
            }
          </ul>
        </div>
      );
    }
  }
}

export default Subcat;