import React from "react";
import Autosuggest from 'react-autosuggest';
var match = require('autosuggest-highlight/match');
var parse = require('autosuggest-highlight/parse');
import { Button, Row, Col } from "reactstrap";


class Search extends React.Component {
  constructor() {
    super();

    this.state = {
      value: '',
      suggestions: [],
      ref: ""
    };
  }

  escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }

  getSuggestions(value) {
    const escapedValue = this.escapeRegexCharacters(value.trim());

    if (escapedValue === '') {
      return [];
    }

    const regex = new RegExp('^' + escapedValue, 'i');

    return this.props.dpr.filter(language => regex.test(language.name));
  }

  getSuggestionValue(suggestion) {
    return suggestion.name;
  }

  renderSuggestion(suggestion, { query }) {
    const matches = match(suggestion.name, query);
    const parts = parse(suggestion.name, matches);

    return (
      <span>
        {parts.map((part, index) => {
          const className = part.highlight ? 'react-autosuggest__suggestion-match' : null;

          return (
            <span className={className} key={index}>
              {part.text}
            </span>
          );
        })}
      </span>
    );
  }

  onChange = (event, { newValue, method }) => {
      this.props.dpr.forEach(element => {
        if (element.name == newValue) {
          this.setState({
            ref: element.ref
          })
        }
      });
    this.setState({
      value: newValue
    });
  };

  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value)
    });
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  handleAdd = () => {
    let obj = {
      name: this.state.value,
      ref: this.state.ref
    }
    this.props.addDpr(obj);
    this.setState({
      value: "",
      ref: ""
    })
  }

  render() {
    const { value, ref, suggestions } = this.state;
    
    const inputProps = {
      placeholder: "Search",
      value,
      ref,
      onChange: this.onChange
    };
    
    return (
      <Row noGutters >
        <Col xs={10} >
          <Autosuggest
            suggestions={suggestions}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
            getSuggestionValue={this.getSuggestionValue}
            renderSuggestion={this.renderSuggestion}
            inputProps={inputProps}
          />
        </Col>
        <Col xs={2} style={{ paddingLeft: 10, marginTop: 7 }}>
          <Button size="sm" onClick={this.handleAdd} >
            <i className="fa fa-plus-circle" style={{ color: "white" }} ></i>
          </Button>
        </Col>
      </Row>
    );
  }
}

export default Search;


// https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters




// import { Link } from "react-router-dom";

// function escapeRegexCharacters(str) {
//   return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
// }

// function getSuggestions(value) {
//   const escapedValue = escapeRegexCharacters(value.trim());

//   if (escapedValue === '') {
//     return [];
//   }

//   const regex = new RegExp('^' + escapedValue, 'i');

//   return languages.filter(language => regex.test(language.name));
// }

// function getSuggestionValue(suggestion) {
//   return suggestion.name;
// }

// function renderSuggestion(suggestion, { query }) {
//   const matches = match(suggestion.name, query);
//   const parts = parse(suggestion.name, matches);

//   return (
//     <span>
//       {parts.map((part, index) => {
//         const className = part.highlight ? 'react-autosuggest__suggestion-match' : null;

//         return (
//           <span className={className} key={index}>
//             {part.text}
//           </span>
//         );
//       })}
//     </span>
//   );
// }