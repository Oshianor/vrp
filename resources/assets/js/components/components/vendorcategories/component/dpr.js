import React, { Component } from 'react';
import axios from 'axios';
import { Container, Row, Col } from "reactstrap";
import PreloaderIcon from 'react-preloader-icon';
import Oval from 'react-preloader-icon/loaders/Oval';
// import Search from './search';
import { Button } from "reactstrap";
import Searchandselect from './searchandselect';


class Dpr extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      dpr: [],
      loadedDpr: []
    }
  }

  componentDidMount() {
    axios.get(this.props.base + '/api/dpr')
      .then(res => {
        const dpr = res.data;
        this.setState({
          dpr,
          loading: false
        });
      })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.dpr !== this.state.dpr) {
      if (typeof this.state.dpr[0] !== "undefined") {
        axios.get(this.props.base + '/get/categories/' + this.props.vendorId)
          .then(res => {
            const loadedDpr = res.data;
            this.setState({
              loadedDpr,
            });
          })
      }
    }
    if (prevState.loadedDpr !== this.state.loadedDpr) {
      if (typeof this.state.loadedDpr[0] !== "undefined") {
        let sim = []
        this.state.dpr.forEach((dp, index) => {
          this.state.loadedDpr.forEach((load) => {
            if (load.dpr_ref === dp.ref) {
              // this.props.addDpr({ name: dp.name, value: dp.ref });
              sim = sim.concat({ name: dp.name, value: dp.ref });
            }
          })
        })
        this.props.pushTo(sim);
      }
    }
  }


  handleSetDpr(ref) {
    this.props.setCurrentDprRef(ref);
  }

  handleDelete(id) {
    this.props.deleteDpr(id);
  }

  render() {
    // console.log("DPR +> ", this.state);

    let sty = {
      marginLeft: -44,
      paddingBottom: 17,
      cursor: "pointer"
    };
    let current = {
      marginLeft: -52,
      marginRight: -13,
      // border: "1px dotted black",
      borderRadius: 2,
      padding: 4,
      marginBottom: "10px",
      marginTop: "5px",
      paddingLeft: 12,
      cursor: "pointer",
      boxShadow: "2px 2px 9px -2px grey"
    }
    if (this.state.loading) {
      return (
        <Container style={{ width: 150 }} >
          <PreloaderIcon
            loader={Oval}
            size={60}
            strokeWidth={8}
            strokeColor="#006064"
            duration={800}
          />
          <p style={{ marginLeft: -10 }} >Give Us A Sec.</p>
        </Container>
      );
    } else {
      return (
        <div>
          <ul style={{ listStyleType: "none" }} >
            {
              this.props.selectedDpr.map((select, index) => (
                <li
                  key={index}
                  style={this.props.currentDprRef === select.value ? current : sty}
                  // onClick={this.handleSetDpr.bind(this, select.value)}
                >
                  <Row>
                    <Col xs={10} onClick={this.handleSetDpr.bind(this, select.value)} >
                      <span>{select.name}</span>
                    </Col>
                    <Col xs={2} >
                      <Button size="sm" color="danger" onClick={this.handleDelete.bind(this, index)} >
                        <i className="fa fa-minus-circle"></i>
                      </Button>
                    </Col>
                  </Row>
                </li>
              ))
            }
          </ul>
          {/* <Search dpr={this.state.dpr} addDpr={this.props.addDpr} /> */}
          <Searchandselect
            addDpr={this.props.addDpr}
            dpr={this.state.dpr}
          />
        </div>
      );
    }
  }
}

export default Dpr;