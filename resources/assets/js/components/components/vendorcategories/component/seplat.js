import React, { Component } from 'react';
import axios from 'axios';
import { Button, FormGroup, Label, Input } from 'reactstrap';
import { Container, Row, Col } from "reactstrap";
import PreloaderIcon from 'react-preloader-icon';
import Oval from 'react-preloader-icon/loaders/Oval';

class Seplat extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dprRef: "",
      seplat: [],
      loading: true,
      select: []
    }
  }

  componentDidUpdate() {
    if (this.state.dprRef !== this.props.currentDprRef) {
      this.setState({
        dprRef: this.props.currentDprRef,
        loading: true
      })
      if (this.props.currentDprRef !== "") {
        axios.get(this.props.base + '/api/seplat/' + this.props.currentDprRef)
          .then(res => {
            const seplat = res.data;
            this.setState({
              seplat,
              loading: false
            });
          })


        // fetch another
        let vendorId = this.props.vendorId;
        let dpr = this.props.currentDprRef;
        axios.get(this.props.base + '/subcat/' + vendorId + "/" + dpr).then(res => {
          if (res.data !== "") {
            this.setState({
              select: res.data,
            });
          }

        })
      }
      
      
        
    }
    
  }

  handleViewSeplat(sep) {
    this.props.setCurrentSeplat(sep);
  }

  handleChecked = (e) => {
    let vendorId = this.props.vendorId;
    let dpr = this.props.currentDprRef;
    let checked = e.target.value;
    if (e.target.checked) {
      axios.get(this.props.base + '/add/seplat/' + vendorId + "/" + dpr + "/" + checked);
      axios.get(this.props.base + '/subcat/' + vendorId + "/" + dpr).then(res => {
        if (res.data !== "") {
          this.setState({
            select: res.data,
          });
        }

      })
    } else {
      axios.get(this.props.base + '/remove/seplat/' + checked);
      axios.get(this.props.base + '/subcat/' + vendorId + "/" + dpr).then(res => {
        if (res.data !== "") {
          this.setState({
            select: res.data,
          });
        }
      })
    }
  }

  render() {
    let dan = []
    this.state.select.forEach((select, index) => {
      dan = dan.concat(Number(select.seplat_ref));
    })
    
    let syl = {
      marginLeft: -45,
      paddingBottom: 15,
    }

    if (this.state.loading) {
      if (this.state.dprRef === "") {
        return (
          <div>
            NO DPR SELECTED
          </div>
        )
      } else {
        return (
          <Container style={{ width: 250 }} >
            <PreloaderIcon
              loader={Oval}
              size={60}
              strokeWidth={8}
              strokeColor="#006064"
              duration={800}
            />
            <p style={{ marginLeft: -10 }} >Loading Seplat Details.</p>
          </Container>
        );
      }
    } else {
      return (
        <div>
          <ul style={{ listStyleType: "none" }}>
            {
              this.state.seplat.map((sep, index) => (
                <li key={index} style={syl} >
                  <FormGroup check>
                    <Row noGutters>
                      <Col xs={1} >
                            <Input
                              key={index}
                              type="checkbox"
                              checked={
                                dan.includes(sep.subcat_ref) ? true : false
                              }
                              onChange={this.handleChecked}
                              value={sep.subcat_ref}
                            />

                        </Col>
                      <Col xs={9} >
                        <span style={{ marginLeft: -25 }} >{sep.name}</span>
                      </Col>
                      <Col xs={2} >
                        <Button
                          size="sm"
                          color={this.props.currentSeplat === sep.subcat_ref ? "success" : "default"}
                          onClick={this.handleViewSeplat.bind(this, sep.subcat_ref)}
                        >
                          view
                        </Button>
                      </Col>
                    </Row>
                  </FormGroup>
                </li>
              ))
            }
          </ul>
        </div>
      );
    }
  }
}

export default Seplat;