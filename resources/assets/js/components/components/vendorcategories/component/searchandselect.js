import React, { Component } from 'react';
import SelectSearch from 'react-select-search';

class Searchandselect extends Component {
  // constructor(props) {
  //   super(props);

  //   this.state = {
  //     loading: true,
  //     dpr: [],
  //   }
  // }

  // componentDidMount() {
  //   axios.get('http://127.0.0.1:8000/api/dpr')
  //     .then(res => {
  //       const dpr = res.data;
  //       this.setState({
  //         dpr,
  //         loading: false
  //       });
  //     })
  // }

  handleAdd = (value, state, props) => {
    this.props.addDpr(value);
  }

  render() {
    let options = [];
    this.props.dpr.forEach((dp) => {
      options = options.concat({name: dp.name, value: dp.ref})
    })
    return (
      <div>
        {
          typeof options[0] !== "undefined" &&
          <SelectSearch
            options={options}
            onChange={this.handleAdd}
            placeholder="Search or Select an option"
      />
    }
    
      </div>
    );
  }
}

export default Searchandselect;