import React, { Component } from 'react';
import { Link } from "react-router-dom";
import {
  Nav,
  NavItem,
  Navbar,
  NavbarBrand,
  Container
} from 'reactstrap';

class Home extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      home: false,
      info: false
    }
  }

  render() {
    return (
      <div>
        <Navbar color="dark" light fixed="top" expand="md">
        <Container>
          <NavbarBrand href="/" style={{ color: "white" }} >
          <img src="/logo.png" alt="logo" width={90} />
            <span style={{ marginTop: 20, fontSize: 14 }} >Vendor Registeration Portal</span>
          </NavbarBrand>
          <Nav className="float-right" >
            <NavItem>
              <Link style={{ textDecoration: "none", paddingRight: 20, color: "white" }} to="/" >Home</Link>
            </NavItem>
            <NavItem>
              <Link style={{ textDecoration: "none", paddingRight: 20, color: "white" }} to="/information">Instructions</Link>
            </NavItem>
          </Nav>
          </Container>
        </Navbar>
      </div>
    );
  }
}
export default Home;