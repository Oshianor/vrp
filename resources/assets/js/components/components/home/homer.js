import React, { Component } from 'react';
import ReactDOM from "react-dom";
import {
  Container,
  Row,
  Col,
  FormGroup,
  Input,
  FormText,
  Alert
} from 'reactstrap';
import PreloaderIcon from 'react-preloader-icon';
import Oval from 'react-preloader-icon/loaders/Oval';
import axios from 'axios';
import { Redirect } from "react-router-dom";

class Homer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      error: "",
      color: "",
      redirect: false,
      detail: "",
      visible: false,
      loading: false
    }
  }

  componentDidMount() {
    ReactDOM.findDOMNode(this.refs.one).focus();
  }

  handleChange() {
    let one = ReactDOM.findDOMNode(this.refs.one).value;
    let two = ReactDOM.findDOMNode(this.refs.two).value;
    let three = ReactDOM.findDOMNode(this.refs.three).value;
    let four = ReactDOM.findDOMNode(this.refs.four).value;
    let five = ReactDOM.findDOMNode(this.refs.five).value;
    let six = ReactDOM.findDOMNode(this.refs.six).value;
    let reg = /[^0-9]/g;
    if (one.length > 0) {
      if (one.length == 1) {
        if (reg.test(one)) {
          ReactDOM.findDOMNode(this.refs.one).value = "";
        } else {
          ReactDOM.findDOMNode(this.refs.two).focus();
        }
      } else if (one.length > 1) {
        ReactDOM.findDOMNode(this.refs.one).value = one.substr(-1);
      }
    }
    if (two.length > 0) {
      if (two.length == 1) {
        if (reg.test(two)) {
          ReactDOM.findDOMNode(this.refs.two).value = "";
        } else {
          ReactDOM.findDOMNode(this.refs.three).focus();
        }
      } else if (two.length > 1) {
        ReactDOM.findDOMNode(this.refs.two).value = two.substr(-1);
      }
    }
    if (three.length > 0) {
      if (three.length == 1) {
        if (reg.test(three)) {
          ReactDOM.findDOMNode(this.refs.three).value = "";
        } else {
          ReactDOM.findDOMNode(this.refs.four).focus();
        }
      } else if (three.length > 1) {
        ReactDOM.findDOMNode(this.refs.three).value = three.substr(-1);
      }
    }
    if (four.length > 0) {
      if (four.length == 1) {
        if (reg.test(four)) {
          ReactDOM.findDOMNode(this.refs.four).value = "";
        } else {
          ReactDOM.findDOMNode(this.refs.five).focus();
        }
      } else if (four.length > 1) {
        ReactDOM.findDOMNode(this.refs.four).value = four.substr(-1);
      }
    }
    if (five.length > 0) {
      if (five.length == 1) {
        if (reg.test(five)) {
          ReactDOM.findDOMNode(this.refs.five).value = "";
        } else {
          ReactDOM.findDOMNode(this.refs.six).focus();
        }
      } else if (five.length > 1) {
        ReactDOM.findDOMNode(this.refs.five).value = five.substr(-1);
      }
    }
    if (six.length > 0) {
      if (six.length == 1) {
        if (reg.test(six)) {
          ReactDOM.findDOMNode(this.refs.six).value = "";
        } else {
          if (one.length < 1) {
            ReactDOM.findDOMNode(this.refs.one).focus();
          }
          if (two.length < 1) {
            ReactDOM.findDOMNode(this.refs.two).focus();
          }
          if (three.length < 1) {
            ReactDOM.findDOMNode(this.refs.three).focus();
          }
          if (four.length < 1) {
            ReactDOM.findDOMNode(this.refs.four).focus();
          }
          if (five.length < 1) {
            ReactDOM.findDOMNode(this.refs.five).focus();
          }
        }
      } else if (six.length > 1) {
        ReactDOM.findDOMNode(this.refs.six).value = six.substr(-1);
      }
    }
    if (one.length === 1 && two.length === 1 && three.length === 1 && four.length === 1 && five.length === 1 && six.length === 1) {
      this.setState({
        loading: true
      })
      let token = one + two + three + four + five + six;
      axios.post(this.props.base + "/login", { token })
        .then(res => {
          // console.log(res);
          
          if (res.data === "") {
            this.setState({
              redirect: false,
              visible: true,
              error: "Incorrect Vendor Code. Send Us An Email At Seplatrevalidation@gmail.com",
              color: "danger",
              loading: false
            })
            ReactDOM.findDOMNode(this.refs.one).value = "";
            ReactDOM.findDOMNode(this.refs.two).value = "";
            ReactDOM.findDOMNode(this.refs.three).value = "";
            ReactDOM.findDOMNode(this.refs.four).value = "";
            ReactDOM.findDOMNode(this.refs.five).value = "";
            ReactDOM.findDOMNode(this.refs.six).value = "";
            ReactDOM.findDOMNode(this.refs.one).focus();
          } else {
            this.setState({
              redirect: true,
              detail: res.data,
              visible: false,
              error: "",
              color: "",
              loading: false
            })
          }
        })
    }
  };

  onDismiss = () => {
    this.setState({
      visible: false
    })
  }

  render() {
    return (
      <Container style={{ width: 400, marginTop: 100 }} >
        <h2 style={{ fontSize: 26 }} >ENTER 6-DIGIT VENDOR CODE</h2>
          <Alert color={this.state.color} isOpen={this.state.visible} toggle={this.onDismiss}>{this.state.error}</Alert>
        <Row noGutters>
          <Col xs={2} sm={2} md={2} lg={2} xl={2} >
            <FormGroup>
              <Input
                className="init"
                type="text"
                required
                ref="one"
                maxLength="1"
                disabled={this.state.login ? true : false}
                onChange={this.handleChange.bind(this)} />
            </FormGroup>
          </Col>
          <Col xs={2} sm={2} md={2} lg={2} xl={2} >
            <FormGroup>
              <Input
                className="init"
                type="text"
                required
                ref="two"
                maxLength="1"
                disabled={this.state.login ? true : false}
                onChange={this.handleChange.bind(this)} />
            </FormGroup>
          </Col>
          <Col xs={2} sm={2} md={2} lg={2} xl={2} >
            <FormGroup>
              <Input
                className="init"
                type="text"
                required
                ref="three"
                maxLength="1"
                disabled={this.state.login ? true : false}
                onChange={this.handleChange.bind(this)} />
            </FormGroup>
          </Col>
          <Col xs={2} sm={2} md={2} lg={2} xl={2} >
            <FormGroup>
              <Input
                className="init"
                type="text"
                required
                ref="four"
                maxLength="1"
                disabled={this.state.login ? true : false}
                onChange={this.handleChange.bind(this)} />
            </FormGroup>
          </Col>
          <Col xs={2} sm={2} md={2} lg={2} xl={2} >
            <FormGroup>
              <Input
                className="init"
                type="text"
                required
                ref="five"
                maxLength="1"
                disabled={this.state.login ? true : false}
                onChange={this.handleChange.bind(this)} />
            </FormGroup>
          </Col>
          <Col xs={2} sm={2} md={2} lg={2} xl={2} >
            <FormGroup>
              <Input
                className="init"
                type="text"
                required
                ref="six"
                maxLength="1"
                disabled={this.state.login ? true : false}
                onChange={this.handleChange.bind(this)} />
            </FormGroup>
          </Col>
        </Row>
        <FormText className="text-center" color="muted">
          If you have not received a code, send a email to <a style={{ color: "#c42020ed" }} href="mailto:seplatrevalidation@gmail.com">seplatrevalidation@gmail.com</a>
        </FormText>
        {
          this.state.loading &&
          <Container style={{ width: 150 }} >
            <PreloaderIcon
              loader={Oval}
              size={60}
              strokeWidth={8}
              strokeColor="#006064"
              duration={800}
            />
            <p style={{ marginLeft: -10 }} >Give Us A Sec.</p>
          </Container>
        }
        {
          this.state.redirect &&
          <Redirect
            to={{
              pathname: "/company/" + this.state.detail.id,
              state: { vendor: this.state.detail }
            }}
          />
        }
      </Container>
    );
  }
}

export default Homer;