import React, { Component } from 'react';
import { Form, FormGroup, Label, Button, Input } from 'reactstrap';
import axios from 'axios';
import ReactDOM from "react-dom";
import PreloaderIcon from 'react-preloader-icon';
import Spinning from 'react-preloader-icon/loaders/Spinning';

class Private extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cvrf: [],
      bankRef: [],
      bankDetails: [],
      lastAudit: [],
      // status
      cvrfStatus: "",
      bankRefStatus: "",
      bankDetailsStatus: "",
      lastAuditStatus: "",
    }
  }

  handleSubmit = () => {
    let cvrf = ReactDOM.findDOMNode(this.refs.cvrf).files;
    if (typeof cvrf[0] !== "undefined") {
      const cvrfData = new FormData();
      cvrfData.append('vendor', this.props.name);
      for (var i = 0; i < cvrf.length; i++) {
        let file = cvrf[i];
        cvrfData.append('cvrf[' + i + ']', file);
      }
      this.setState({
        cvrfStatus: "waiting"
      })
      axios.post(this.props.base + "/cvrf", cvrfData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }).then(res => {
          // console.log(res);

          if (res.status === 200) {
            ReactDOM.findDOMNode(this.refs.cvrf).value = "";
            this.setState({
              cvrfStatus: "completed"
            })
          } else {
            ReactDOM.findDOMNode(this.refs.cvrf).value = "";
            this.setState({
              cvrfStatus: "failed",
              cvrf: []
            })
          }
        });
    } else {
      this.setState({
        cvrfStatus: "completed"
      })
    }
  }

  componentDidUpdate(prevProp, prevState) {
    if (prevState.cvrfStatus !== this.state.cvrfStatus) {
      if (this.state.cvrfStatus === "completed") {
        let bankRef = ReactDOM.findDOMNode(this.refs.bankRef).files;
        if (typeof bankRef[0] !== "undefined") {
          const bankRefData = new FormData();
          bankRefData.append('vendor', this.props.name);
          for (var i = 0; i < bankRef.length; i++) {
            let file = bankRef[i];
            bankRefData.append('bankRef[' + i + ']', file);
          }
          this.setState({
            bankRefStatus: "waiting"
          })
          axios.post(this.props.base + "/bankRef", bankRefData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.bankRef).value = "";
                this.setState({
                  bankRefStatus: "completed"
                })
              } else {
                this.setState({
                  bankRefStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            bankRefStatus: "completed"
          })
        }

      }
    }


    if (prevState.bankRefStatus !== this.state.bankRefStatus) {
      if (this.state.bankRefStatus === "completed") {
        let bankDetails = ReactDOM.findDOMNode(this.refs.bankDetails).files;
        if (typeof bankDetails[0] !== "undefined") {
          const bankDetailsData = new FormData();
          bankDetailsData.append('vendor', this.props.name);
          for (var i = 0; i < bankDetails.length; i++) {
            let file = bankDetails[i];
            bankDetailsData.append('bankDetails[' + i + ']', file);
          }
          this.setState({
            bankDetailsStatus: "waiting"
          })
          axios.post(this.props.base + "/bankDetails", bankDetailsData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.bankDetails).value = "";
                this.setState({
                  bankDetailsStatus: "completed"
                })
              } else {
                this.setState({
                  bankDetailsStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            bankDetailsStatus: "completed"
          })
        }

      }
    }


    if (prevState.bankDetailsStatus !== this.state.bankDetailsStatus) {
      if (this.state.bankDetailsStatus === "completed") {
        let lastAudit = ReactDOM.findDOMNode(this.refs.lastAudit).files;
        if (typeof lastAudit[0] !== "undefined") {
          const lastAuditData = new FormData();
          lastAuditData.append('vendor', this.props.name);
          for (var i = 0; i < lastAudit.length; i++) {
            let file = lastAudit[i];
            lastAuditData.append('lastAudit[' + i + ']', file);
          }
          this.setState({
            lastAuditStatus: "waiting"
          })
          axios.post(this.props.base + "/lastAudit", lastAuditData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.lastAudit).value = "";
                this.setState({
                  lastAuditStatus: "completed",
                  cvrfStatus: ""
                })
              } else {
                this.setState({
                  lastAuditStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            lastAuditStatus: "completed",
            cvrfStatus: ""
          })
        }

      }
    }

  }


  // functions to preview
  cvrf = (event) => {
    let self = this;
    this.setState({
      cvrf: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          cvrf: self.state.cvrf.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  bankRef = (event) => {
    let self = this;
    this.setState({
      bankRef: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          bankRef: self.state.bankRef.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  bankDetails = (event) => {
    let self = this;
    this.setState({
      bankDetails: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          bankDetails: self.state.bankDetails.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  lastAudit = (event) => {
    let self = this;
    this.setState({
      lastAudit: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          lastAudit: self.state.lastAudit.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }

  render() {
    console.log(this.state);

    return (
      <Form>
        <div>
          {
            typeof this.state.cvrf[0] !== "undefined" &&
            this.state.cvrf.map((cv, index) => (
              <img
                key={index}
                src={cv}
                alt="Completed Vendor Registration Form"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label className="heed">
              Completed Vendor Registration Form
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.cvrfStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.cvrfStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.cvrfStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="cvrf" onChange={this.cvrf} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.bankRef[0] !== "undefined" &&
            this.state.bankRef.map((ref, index) => (
              <img
                key={index}
                src={ref}
                alt="Bank Reference Letter"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label className="heed" >
              Bank Reference Letter
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.bankRefStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.bankRefStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.bankRefStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="bankRef" onChange={this.bankRef} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.bankDetails[0] !== "undefined" &&
            this.state.bankDetails.map((del, index) => (
              <img
                key={index}
                src={del}
                alt="Bank details on official letterhead, duly signed"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label for="exampleFile" className="heed" >
              Bank details on official letterhead, duly signed
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.bankDetailsStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.bankDetailsStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.bankDetailsStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="bankDetails" id="exampleFile" onChange={this.bankDetails} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.lastAudit[0] !== "undefined" &&
            this.state.lastAudit.map((last, index) => (
              <img
                key={index}
                src={last}
                alt="Last Audited accounts / Statement of affairs"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label for="exampleCustomFileBrowser" className="heed" >
              Last Audited accounts / Statement of affairs
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.lastAuditStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.lastAuditStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.lastAuditStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="lastAudit" id="exampleFile" onChange={this.lastAudit} />
          </FormGroup>
        </div>

        <Button
          color="danger"
          size="lg"
          onClick={this.handleSubmit}
          disabled={
            this.state.cvrfStatus !== "" ? true : false
          }
        >
          Submit
        </Button>
      </Form>
    );
  }
}

export default Private;