import React, { Component } from 'react';
import { Form, FormGroup, Label, Button, Input } from 'reactstrap';
import axios from 'axios';
import ReactDOM from "react-dom";
import PreloaderIcon from 'react-preloader-icon';
import Spinning from 'react-preloader-icon/loaders/Spinning';

class Private extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cvrf: [],
      certIncorp: [],
      formCOTWO: [],
      memorandum: [],
      proofTax: [],
      incomeTax: [],
      companyProfile: [],
      proofMedical: [],
      bankRef: [],
      bankDetails: [],
      lastAudit: [],
      workmen: [],
      thirdParty: [],
      hssePolicy: [],
      assurance: [],
      // status
      cvrfStatus: "",
      certIncorpStatus: "",
      formCOTWOStatus: "",
      memorandumStatus: "",
      proofTaxStatus: "",
      incomeTaxStatus: "",
      companyProfileStatus: "",
      proofMedicalStatus: "",
      bankRefStatus: "",
      bankDetailsStatus: "",
      lastAuditStatus: "",
      workmenStatus: "",
      thirdPartyStatus: "",
      hssePolicyStatus: "",
      assuranceStatus: "",
    }
  }

  handleSubmit = () => {
    let cvrf = ReactDOM.findDOMNode(this.refs.cvrf).files;
    if (typeof cvrf[0] !== "undefined") {
      const cvrfData = new FormData();
      cvrfData.append('vendor', this.props.name);
      for (var i = 0; i < cvrf.length; i++) {
        let file = cvrf[i];
        cvrfData.append('cvrf[' + i + ']', file);
      }
      this.setState({
        cvrfStatus: "waiting"
      })
      axios.post(this.props.base + "/cvrf", cvrfData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }).then(res => {
          // console.log(res);

          if (res.status === 200) {
            ReactDOM.findDOMNode(this.refs.cvrf).value = "";
            this.setState({
              cvrfStatus: "completed"
            })
          } else {
            ReactDOM.findDOMNode(this.refs.cvrf).value = "";
            this.setState({
              cvrfStatus: "failed",
              cvrf: []
            })
          }
        });
    } else {
      this.setState({
        cvrfStatus: "completed"
      })
    }
  }

  componentDidUpdate(prevProp, prevState) {
    if (prevState.cvrfStatus !== this.state.cvrfStatus) {
      if (this.state.cvrfStatus === "completed") {
        let certIncorp = ReactDOM.findDOMNode(this.refs.certIncorp).files;
        if (typeof certIncorp[0] !== "undefined") {
          const certIncorpData = new FormData();
          certIncorpData.append('vendor', this.props.name);
          for (var i = 0; i < certIncorp.length; i++) {
            let file = certIncorp[i];
            certIncorpData.append('certIncorp[' + i + ']', file);
          }
          this.setState({
            certIncorpStatus: "waiting"
          })
          axios.post(this.props.base + "/certIncorp", certIncorpData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.certIncorp).value = "";
                this.setState({
                  certIncorpStatus: "completed",
                })
              } else {
                ReactDOM.findDOMNode(this.refs.certIncorp).value = "";
                this.setState({
                  certIncorpStatus: "failed",
                  certIncorp: []
                })
              }
            })
        } else {
          this.setState({
            certIncorpStatus: "completed"
          })
        }
      }
    }


    if (prevState.certIncorpStatus !== this.state.certIncorpStatus) {
      let formCOTWO = ReactDOM.findDOMNode(this.refs.formCOTWO).files;
      if (this.state.certIncorpStatus === "completed") {
        if (typeof formCOTWO[0] !== "undefined") {
          const formCOTWOData = new FormData();
          formCOTWOData.append('vendor', this.props.name);
          for (var i = 0; i < formCOTWO.length; i++) {
            let file = formCOTWO[i];
            formCOTWOData.append('formCOTWO[' + i + ']', file);
          }
          this.setState({
            formCOTWOStatus: "waiting"
          })
          axios.post(this.props.base + "/formCOTWO", formCOTWOData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.formCOTWO).value = "";
                this.setState({
                  formCOTWOStatus: "completed"
                })
              } else {
                this.setState({
                  formCOTWOStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            formCOTWOStatus: "completed"
          })
        }
      }
    }


    if (prevState.formCOTWOStatus !== this.state.formCOTWOStatus) {
      if (this.state.formCOTWOStatus === "completed") {
        let memorandum = ReactDOM.findDOMNode(this.refs.memorandum).files;
        if (typeof memorandum[0] !== "undefined") {
          const memorandumData = new FormData();
          memorandumData.append('vendor', this.props.name);
          for (var i = 0; i < memorandum.length; i++) {
            let file = memorandum[i];
            memorandumData.append('memorandum[' + i + ']', file);
          }
          this.setState({
            memorandumStatus: "waiting"
          })
          axios.post(this.props.base + "/memorandum", memorandumData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.memorandum).value = "";
                this.setState({
                  memorandumStatus: "completed"
                })
              } else {
                this.setState({
                  memorandumStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            memorandumStatus: "completed"
          })
        }
      }
    }


    if (prevState.memorandumStatus !== this.state.memorandumStatus) {
      if (this.state.memorandumStatus === "completed") {
        let proofTax = ReactDOM.findDOMNode(this.refs.proofTax).files;
        if (typeof proofTax[0] !== "undefined") {
          const proofTaxData = new FormData();
          proofTaxData.append('vendor', this.props.name);
          for (var i = 0; i < proofTax.length; i++) {
            let file = proofTax[i];
            proofTaxData.append('proofTax[' + i + ']', file);
          }
          this.setState({
            proofTaxStatus: "waiting"
          })
          axios.post(this.props.base + "/proofTax", proofTaxData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.proofTax).value = "";
                this.setState({
                  proofTaxStatus: "completed"
                })
              } else {
                this.setState({
                  proofTaxStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            proofTaxStatus: "completed"
          })
        }
      }
    }


    if (prevState.proofTaxStatus !== this.state.proofTaxStatus) {
      if (this.state.proofTaxStatus === "completed") {
        let incomeTax = ReactDOM.findDOMNode(this.refs.incomeTax).files;
        if (typeof incomeTax[0] !== "undefined") {
          const incomeTaxData = new FormData();
          incomeTaxData.append('vendor', this.props.name);
          for (var i = 0; i < incomeTax.length; i++) {
            let file = incomeTax[i];
            incomeTaxData.append('incomeTax[' + i + ']', file);
          }
          this.setState({
            incomeTaxStatus: "waiting"
          })
          axios.post(this.props.base + "/incomeTax", incomeTaxData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.incomeTax).value = "";
                this.setState({
                  incomeTaxStatus: "completed"
                })
              } else {
                this.setState({
                  incomeTaxStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            incomeTaxStatus: "completed"
          })
        }

      }
    }


    if (prevState.incomeTaxStatus !== this.state.incomeTaxStatus) {
      if (this.state.incomeTaxStatus === "completed") {
        let companyProfile = ReactDOM.findDOMNode(this.refs.companyProfile).files;
        if (typeof companyProfile[0] !== "undefined") {
          const companyProfileData = new FormData();
          companyProfileData.append('vendor', this.props.name);
          for (var i = 0; i < companyProfile.length; i++) {
            let file = companyProfile[i];
            companyProfileData.append('companyProfile[' + i + ']', file);
          }
          this.setState({
            companyProfileStatus: "waiting"
          })
          axios.post(this.props.base + "/companyProfile", companyProfileData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.companyProfile).value = "";
                this.setState({
                  companyProfileStatus: "completed"
                })
              } else {
                this.setState({
                  companyProfileStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            companyProfileStatus: "completed"
          })
        }

      }
    }


    if (prevState.companyProfileStatus !== this.state.companyProfileStatus) {
      if (this.state.companyProfileStatus === "completed") {
        let proofMedical = ReactDOM.findDOMNode(this.refs.proofTax).files;
        if (typeof proofMedical[0] !== "undefined") {
          const proofMedicalData = new FormData();
          proofMedicalData.append('vendor', this.props.name);
          for (var i = 0; i < proofMedical.length; i++) {
            let file = proofMedical[i];
            proofMedicalData.append('proofMedical[' + i + ']', file);
          }
          this.setState({
            proofMedicalStatus: "waiting"
          })
          axios.post(this.props.base + "/proofMedical", proofMedicalData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.proofMedical).value = "";
                this.setState({
                  proofMedicalStatus: "completed"
                })
              } else {
                this.setState({
                  proofMedicalStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            proofMedicalStatus: "completed"
          })
        }

      }
    }



    if (prevState.proofMedicalStatus !== this.state.proofMedicalStatus) {
      if (this.state.proofMedicalStatus === "completed") {
        let bankRef = ReactDOM.findDOMNode(this.refs.bankRef).files;
        if (typeof bankRef[0] !== "undefined") {
          const bankRefData = new FormData();
          bankRefData.append('vendor', this.props.name);
          for (var i = 0; i < bankRef.length; i++) {
            let file = bankRef[i];
            bankRefData.append('bankRef[' + i + ']', file);
          }
          this.setState({
            bankRefStatus: "waiting"
          })
          axios.post(this.props.base + "/bankRef", bankRefData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.bankRef).value = "";
                this.setState({
                  bankRefStatus: "completed"
                })
              } else {
                this.setState({
                  bankRefStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            bankRefStatus: "completed"
          })
        }

      }
    }



    if (prevState.bankRefStatus !== this.state.bankRefStatus) {
      if (this.state.bankRefStatus === "completed") {
        let bankDetails = ReactDOM.findDOMNode(this.refs.bankDetails).files;
        if (typeof bankDetails[0] !== "undefined") {
          const bankDetailsData = new FormData();
          bankDetailsData.append('vendor', this.props.name);
          for (var i = 0; i < bankDetails.length; i++) {
            let file = bankDetails[i];
            bankDetailsData.append('bankDetails[' + i + ']', file);
          }
          this.setState({
            bankDetailsStatus: "waiting"
          })
          axios.post(this.props.base + "/bankDetails", bankDetailsData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.bankDetails).value = "";
                this.setState({
                  bankDetailsStatus: "completed"
                })
              } else {
                this.setState({
                  bankDetailsStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            bankDetailsStatus: "completed"
          })
        }

      }
    }


    if (prevState.bankDetailsStatus !== this.state.bankDetailsStatus) {
      if (this.state.bankDetailsStatus === "completed") {
        let lastAudit = ReactDOM.findDOMNode(this.refs.lastAudit).files;
        if (typeof lastAudit[0] !== "undefined") {
          const lastAuditData = new FormData();
          lastAuditData.append('vendor', this.props.name);
          for (var i = 0; i < lastAudit.length; i++) {
            let file = lastAudit[i];
            lastAuditData.append('lastAudit[' + i + ']', file);
          }
          this.setState({
            lastAuditStatus: "waiting"
          })
          axios.post(this.props.base + "/lastAudit", lastAuditData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.lastAudit).value = "";
                this.setState({
                  lastAuditStatus: "completed",
                })
              } else {
                this.setState({
                  lastAuditStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            lastAuditStatus: "completed",
          })
        }

      }
    }


    if (prevState.lastAuditStatus !== this.state.lastAuditStatus) {
      if (this.state.lastAuditStatus === "completed") {
        let workmen = ReactDOM.findDOMNode(this.refs.workmen).files;
        if (typeof workmen[0] !== "undefined") {
          const workmenData = new FormData();
          workmenData.append('vendor', this.props.name);
          for (var i = 0; i < workmen.length; i++) {
            let file = workmen[i];
            workmenData.append('workmen[' + i + ']', file);
          }
          this.setState({
            workmenStatus: "waiting"
          })
          axios.post(this.props.base + "/workmen", workmenData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.workmen).value = "";
                this.setState({
                  workmenStatus: "completed",
                })
              } else {
                this.setState({
                  workmenStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            workmenStatus: "completed",
          })
        }
      }
    }



    if (prevState.workmenStatus !== this.state.workmenStatus) {
      if (this.state.workmenStatus === "completed") {
        let thirdParty = ReactDOM.findDOMNode(this.refs.thirdParty).files;
        if (typeof thirdParty[0] !== "undefined") {
          const thirdPartyData = new FormData();
          thirdPartyData.append('vendor', this.props.name);
          for (var i = 0; i < thirdParty.length; i++) {
            let file = thirdParty[i];
            thirdPartyData.append('thirdParty[' + i + ']', file);
          }
          this.setState({
            thirdPartyStatus: "waiting"
          })
          axios.post(this.props.base + "/thirdParty", thirdPartyData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.thirdParty).value = "";
                this.setState({
                  thirdPartyStatus: "completed",
                })
              } else {
                this.setState({
                  thirdPartyStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            thirdPartyStatus: "completed",
          })
        }
      }
    }



    if (prevState.thirdPartyStatus !== this.state.thirdPartyStatus) {
      if (this.state.thirdPartyStatus === "completed") {
        let hssePolicy = ReactDOM.findDOMNode(this.refs.hssePolicy).files;
        if (typeof hssePolicy[0] !== "undefined") {
          const hssePolicyData = new FormData();
          hssePolicyData.append('vendor', this.props.name);
          for (var i = 0; i < hssePolicy.length; i++) {
            let file = lastAudit[i];
            hssePolicyData.append('hssePolicy[' + i + ']', file);
          }
          this.setState({
            hssePolicyStatus: "waiting"
          })
          axios.post(this.props.base + "/hssePolicy", hssePolicyData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.hssePolicy).value = "";
                this.setState({
                  hssePolicyStatus: "completed",
                })
              } else {
                this.setState({
                  hssePolicyStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            hssePolicyStatus: "completed",
          })
        }
      }
    }


    if (prevState.hssePolicyStatus !== this.state.hssePolicyStatus) {
      if (this.state.hssePolicyStatus === "completed") {
        let assurance = ReactDOM.findDOMNode(this.refs.assurance).files;
        if (typeof assurance[0] !== "undefined") {
          const assuranceData = new FormData();
          assuranceData.append('vendor', this.props.name);
          for (var i = 0; i < assurance.length; i++) {
            let file = assurance[i];
            assuranceData.append('assurance[' + i + ']', file);
          }
          this.setState({
            assuranceStatus: "waiting"
          })
          axios.post(this.props.base + "/assurance", assuranceData,
            {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(res => {
              if (res.status === 200) {
                ReactDOM.findDOMNode(this.refs.assurance).value = "";
                this.setState({
                  assuranceStatus: "completed",
                  cvrfStatus: ""
                })
              } else {
                this.setState({
                  assuranceStatus: "failed"
                })
              }
            })
        } else {
          this.setState({
            assuranceStatus: "completed",
            cvrfStatus: ""
          })
        }
      }
    }



  }


  // functions to preview
  cvrf = (event) => {
    let self = this;
    this.setState({
      cvrf: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          cvrf: self.state.cvrf.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  certIncorp = (event) => {
    let self = this;
    this.setState({
      certIncorp: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          certIncorp: self.state.certIncorp.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  formCOTWO = (event) => {
    let self = this;
    this.setState({
      formCOTWO: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          formCOTWO: self.state.formCOTWO.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  memorandum = (event) => {
    let self = this;
    this.setState({
      memorandum: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          memorandum: self.state.memorandum.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  proofTax = (event) => {
    let self = this;
    this.setState({
      proofTax: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          proofTax: self.state.proofTax.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  incomeTax = (event) => {
    let self = this;
    this.setState({
      incomeTax: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          incomeTax: self.state.incomeTax.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  companyProfile = (event) => {
    let self = this;
    this.setState({
      companyProfile: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          companyProfile: self.state.companyProfile.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  proofMedical = (event) => {
    let self = this;
    this.setState({
      proofMedical: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          proofMedical: self.state.proofMedical.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  bankRef = (event) => {
    let self = this;
    this.setState({
      bankRef: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          bankRef: self.state.bankRef.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  bankDetails = (event) => {
    let self = this;
    this.setState({
      bankDetails: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          bankDetails: self.state.bankDetails.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  lastAudit = (event) => {
    let self = this;
    this.setState({
      lastAudit: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          lastAudit: self.state.lastAudit.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  workmen = (event) => {
    let self = this;
    this.setState({
      workmen: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          workmen: self.state.workmen.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  thirdParty = (event) => {
    let self = this;
    this.setState({
      thirdParty: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          thirdParty: self.state.thirdParty.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  hssePolicy = (event) => {
    let self = this;
    this.setState({
      hssePolicy: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          hssePolicy: self.state.hssePolicy.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }
  assurance = (event) => {
    let self = this;
    this.setState({
      assurance: []
    })
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      var reader = new FileReader();
      reader.onload = function (e) {
        self.setState({
          assurance: self.state.assurance.concat(e.target.result)
        });

      }
      reader.readAsDataURL(event.target.files[i]);
    }
  }

  render() {
    return (
      <Form>
        <div>
          {
            typeof this.state.cvrf[0] !== "undefined" &&
            this.state.cvrf.map((cv, index) => (
              <img
                key={index}
                src={cv}
                alt="Completed Vendor Registration Form"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label className="heed">
              Completed Vendor Registration Form
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.cvrfStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.cvrfStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.cvrfStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="cvrf" onChange={this.cvrf} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.certIncorp[0] !== "undefined" &&
            this.state.certIncorp.map((cv, index) => (
              <img
                key={index}
                src={cv}
                alt="Completed Vendor Registration Form"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label className="heed">
              Certificate of Incorpoation / Registration
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.certIncorpStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.certIncorpStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.certIncorpStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="certIncorp" onChange={this.certIncorp} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.formCOTWO[0] !== "undefined" &&
            this.state.formCOTWO.map((cv, index) => (
              <img
                key={index}
                src={cv}
                alt="Completed Vendor Registration Form"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label className="heed">
              Form CO2&CO7 or Form 2.3 & 2.5
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.formCOTWOStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.formCOTWOStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.formCOTWOStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="formCOTWO" onChange={this.formCOTWO} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.memorandum[0] !== "undefined" &&
            this.state.memorandum.map((cv, index) => (
              <img
                key={index}
                src={cv}
                alt="Completed Vendor Registration Form"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label className="heed">
              Memorandum & Articles of Association
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.memorandumStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.memorandumStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.memorandumStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="memorandum" onChange={this.memorandum} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.proofTax[0] !== "undefined" &&
            this.state.proofTax.map((cv, index) => (
              <img
                key={index}
                src={cv}
                alt="Completed Vendor Registration Form"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label className="heed">
              Proof of Tax Identification Number (TIN)
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.proofTaxStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.proofTaxStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.proofTaxStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="proofTax" onChange={this.proofTax} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.incomeTax[0] !== "undefined" &&
            this.state.incomeTax.map((cv, index) => (
              <img
                key={index}
                src={cv}
                alt="Completed Vendor Registration Form"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label className="heed">
              Income tax clearance for the past 3 years
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.incomeTaxStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.incomeTaxStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.incomeTaxStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="incomeTax" onChange={this.incomeTax} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.companyProfile[0] !== "undefined" &&
            this.state.companyProfile.map((cv, index) => (
              <img
                key={index}
                src={cv}
                alt="Completed Vendor Registration Form"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label className="heed">
              Company profile showing Org Chart/CV of Key personnel
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.companyProfileStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.companyProfileStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.companyProfileStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="companyProfile" onChange={this.companyProfile} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.proofMedical[0] !== "undefined" &&
            this.state.proofMedical.map((cv, index) => (
              <img
                key={index}
                src={cv}
                alt="Completed Vendor Registration Form"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label className="heed">
              Proof of Medical Retainership
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.proofMedicalStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.proofMedicalStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.proofMedicalStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="proofMedical" onChange={this.proofMedical} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.bankRef[0] !== "undefined" &&
            this.state.bankRef.map((ref, index) => (
              <img
                key={index}
                src={ref}
                alt="Bank Reference Letter"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label className="heed" >
              Bank Reference Letter
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.bankRefStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.bankRefStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.bankRefStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="bankRef" onChange={this.bankRef} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.bankDetails[0] !== "undefined" &&
            this.state.bankDetails.map((del, index) => (
              <img
                key={index}
                src={del}
                alt="Bank details on official letterhead, duly signed"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label for="exampleFile" className="heed" >
              Bank details on official letterhead, duly signed
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.bankDetailsStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.bankDetailsStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.bankDetailsStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="bankDetails" id="exampleFile" onChange={this.bankDetails} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.lastAudit[0] !== "undefined" &&
            this.state.lastAudit.map((last, index) => (
              <img
                key={index}
                src={last}
                alt="Last Audited accounts / Statement of affairs"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label for="exampleCustomFileBrowser" className="heed" >
              Last Audited accounts / Statement of affairs
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.lastAuditStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.lastAuditStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.lastAuditStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="lastAudit" id="exampleFile" onChange={this.lastAudit} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.workmen[0] !== "undefined" &&
            this.state.workmen.map((last, index) => (
              <img
                key={index}
                src={last}
                alt="Last Audited accounts / Statement of affairs"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label for="exampleCustomFileBrowser" className="heed" >
              Current workmen compensation or NSITF Registration
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.workmenStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.workmenStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.workmenStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="workmen" id="exampleFile" onChange={this.workmen} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.thirdParty[0] !== "undefined" &&
            this.state.thirdParty.map((last, index) => (
              <img
                key={index}
                src={last}
                alt="Last Audited accounts / Statement of affairs"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label for="exampleCustomFileBrowser" className="heed" >
              General 3rd party insurance coverage
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.thirdPartyStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.thirdPartyStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.thirdPartyStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="thirdParty" id="exampleFile" onChange={this.thirdParty} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.hssePolicy[0] !== "undefined" &&
            this.state.hssePolicy.map((last, index) => (
              <img
                key={index}
                src={last}
                alt="Last Audited accounts / Statement of affairs"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label for="exampleCustomFileBrowser" className="heed" >
              HSSE Policy
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.hssePolicyStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.hssePolicyStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.hssePolicyStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="hssePolicy" id="exampleFile" onChange={this.hssePolicy} />
          </FormGroup>
        </div>

        <div>
          {
            typeof this.state.assurance[0] !== "undefined" &&
            this.state.assurance.map((last, index) => (
              <img
                key={index}
                src={last}
                alt="Quality Assurance / Quality Control Policy"
                width="200"
                height="200"
                className="rounded float-left"
                style={{ marginRight: 10 }}
              />
            ))
          }
          <div className="clearfix"></div>
          <FormGroup>
            <Label for="exampleCustomFileBrowser" className="heed" >
              Quality Assurance / Quality Control Policy
              <span style={{ float: "right", paddingLeft: 10 }} >
                {
                  this.state.assuranceStatus === "waiting" &&
                  <PreloaderIcon
                    loader={Spinning}
                    size={31}
                    strokeWidth={8}
                    strokeColor="red"
                    duration={800}
                  />
                }
                {
                  this.state.assuranceStatus === "completed" &&
                  <i className="fa fa-check fa-2x" style={{ color: "#28a745" }} ></i>
                }
                {
                  this.state.assuranceStatus === "failed" &&
                  <i className="fa fa-times-circle fa-2x" style={{ color: "#dc3545" }} ></i>
                }
              </span>
            </Label>
            <Input type="file" multiple ref="assurance" id="exampleFile" onChange={this.assurance} />
          </FormGroup>
        </div>

        <Button
          color="danger"
          size="lg"
          onClick={this.handleSubmit}
          disabled={
            this.state.cvrfStatus !== "" ? true : false
          }
        >
          Submit
        </Button>
      </Form>
    );
  }
}

export default Private;