<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>Laravel</title>
        <style>
          table, th, td {
              border: 1px solid black;
          }
        </style>
    </head>
    <body>
      <p>Dear Vendor,</p>
      <p>
        We confirm receipt of information and the uploaded documentation from your company.
        The information will be reviewed and feedback will be given within seven (7) business days.
        Find below a summary of the data submitted
      </p>
      <span>
        Company Name: <strong>{{ $vendor }}</strong>
      </span>
      <table style="width:100%" class="text-center" >
        <thead>
          <tr>
            <th>Document Category</th>
            <th>Received</th>
          </tr>
        </thead>
        <tbody>
          
          <tr>
            <td>Completed Vendor Registration Form</td>
            <td>{{ $cvrf }}</td>
          </tr>
          
          @if ($type !== "Gov")
            <tr>
              <td>Certificate of Incorpoation / Registration</td>
              <td> {{ $certIncorp }} </td>
            </tr>
          @endif
          
          @if ($type !== "Gov" && $type !== "Inter" )
            <tr>
              <td>Form CO2&CO7 or Form 2.3 & 2.5</td>
              <td> {{ $formCOTWO }} </td>
            </tr>
          @endif
          
          @if ($type !== "Gov" && $type !== "Inter" )
            <tr>
              <td>Memorandum & Articles of Association</td>
              <td> {{ $memorandum }} </td>
            </tr>
          @endif

          @if ($type !== "Gov" && $type !== "Inter" )
            <tr>
              <td>Proof of Tax Identification Number (TIN)</td>
              <td> {{ $proofTax }} </td>
            </tr>
          @endif

          @if ($type !== "Gov" && $type !== "Inter" )
            <tr>
              <td>Income tax clearance for the past 3 years</td>
              <td> {{ $incomeTax }} </td>
            </tr>
          @endif

          @if ($type !== "Gov")
            <tr>
              <td>Company profile showing Org Chart/CV of Key personnel</td>
              <td> {{ $companyProfile }} </td>
            </tr>
          @endif
          
          @if ($type !== "Gov")
            <tr>
              <td>Proof of Medical Retainership</td>
              <td> {{ $proofMedical }} </td>
            </tr>
          @endif
          
          @if ($type !== "Inter" )
            <tr>
              <td>Bank Reference Letter</td>
              <td> {{ $bankRef }} </td>
            </tr>
          @endif
          
          <tr>
            <td>Bank details on official letterhead, duly signed</td>
            <td> {{ $bankDetails }} </td>
          </tr>
          
          <tr>
            <td>Last Audited accounts / Statement of affairs</td>
            <td> {{ $lastAudit }} </td>
          </tr>

          @if ($type === "Pri" && $type === "Com" )
            <tr>
              <td>DPR Permit</td>
              <td> {{ $dprPermit }} </td>
            </tr>
          @endif

          @if ($type === "Com")
            <tr>
              <td>Introduction Letter from Community</td>
              <td> {{ $introLetter }} </td>
            </tr>
          @endif

          @if ($type !== "Gov" && $type !== "Inter" )
            <tr>
              <td>Current workmen compensation or NSITF Registration</td>
              <td> {{ $workmen }} </td>
            </tr>
          @endif

          @if ($type !== "Gov" && $type !== "Inter" )
            <tr>
              <td>General 3rd party insurance coverage</td>
              <td> {{ $thirdParty }} </td>
            </tr>
          @endif

          @if ($type !== "Gov")
            <tr>
              <td>HSSE Policy</td>
              <td> {{ $hssePolicy }} </td>
            </tr>
          @endif

          @if ($type !== "Gov")
            <tr>
              <td>Quality Assurance / Quality Control Policy</td>
              <td> {{ $assurance }} </td>
            </tr>
          @endif

          @if ($type !== "Gov" && $type !== "Ngo" )
            <tr>
              <td>OEM Agency letter (if applicable)</td>
              <td> {{ $agency }} </td>
            </tr>
          @endif
        
        </tbody>
      </table>
      <p>
        To upload additional/missing documents visit the Seplat Vendor Revalidation Portal.  
        Only add the new/missing documents as previous documents are NOT deleted or overwritten
      </p>
      <p>All enquiries should be directed to us via email at seplatrevalidation@gmail.com.</p>
      <p>Thank you.</p>
      <p>For: Seplat Petroleum Development Company Plc</p>
      <strong>Vendor Management</strong>
    </body>
</html>
